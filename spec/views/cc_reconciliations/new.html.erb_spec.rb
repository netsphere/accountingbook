require 'rails_helper'

RSpec.describe "cc_reconciliations/new", type: :view do
  before(:each) do
    assign(:cc_reconciliation, CcReconciliation.new())
  end

  it "renders new cc_reconciliation form" do
    render

    assert_select "form[action=?][method=?]", cc_reconciliations_path, "post" do
    end
  end
end
