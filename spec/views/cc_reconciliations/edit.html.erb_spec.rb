require 'rails_helper'

RSpec.describe "cc_reconciliations/edit", type: :view do
  let(:cc_reconciliation) {
    CcReconciliation.create!()
  }

  before(:each) do
    assign(:cc_reconciliation, cc_reconciliation)
  end

  it "renders the edit cc_reconciliation form" do
    render

    assert_select "form[action=?][method=?]", cc_reconciliation_path(cc_reconciliation), "post" do
    end
  end
end
