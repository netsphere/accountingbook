require "rails_helper"

RSpec.describe CcReconciliationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/cc_reconciliations").to route_to("cc_reconciliations#index")
    end

    it "routes to #new" do
      expect(get: "/cc_reconciliations/new").to route_to("cc_reconciliations#new")
    end

    it "routes to #show" do
      expect(get: "/cc_reconciliations/1").to route_to("cc_reconciliations#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/cc_reconciliations/1/edit").to route_to("cc_reconciliations#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/cc_reconciliations").to route_to("cc_reconciliations#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/cc_reconciliations/1").to route_to("cc_reconciliations#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/cc_reconciliations/1").to route_to("cc_reconciliations#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/cc_reconciliations/1").to route_to("cc_reconciliations#destroy", id: "1")
    end
  end
end
