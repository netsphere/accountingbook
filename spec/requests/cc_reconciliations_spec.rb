require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe "/cc_reconciliations", type: :request do
  
  # This should return the minimal set of attributes required to create a valid
  # CcReconciliation. As you add validations to CcReconciliation, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  describe "GET /index" do
    it "renders a successful response" do
      CcReconciliation.create! valid_attributes
      get cc_reconciliations_url
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      cc_reconciliation = CcReconciliation.create! valid_attributes
      get cc_reconciliation_url(cc_reconciliation)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "renders a successful response" do
      get new_cc_reconciliation_url
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "renders a successful response" do
      cc_reconciliation = CcReconciliation.create! valid_attributes
      get edit_cc_reconciliation_url(cc_reconciliation)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new CcReconciliation" do
        expect {
          post cc_reconciliations_url, params: { cc_reconciliation: valid_attributes }
        }.to change(CcReconciliation, :count).by(1)
      end

      it "redirects to the created cc_reconciliation" do
        post cc_reconciliations_url, params: { cc_reconciliation: valid_attributes }
        expect(response).to redirect_to(cc_reconciliation_url(CcReconciliation.last))
      end
    end

    context "with invalid parameters" do
      it "does not create a new CcReconciliation" do
        expect {
          post cc_reconciliations_url, params: { cc_reconciliation: invalid_attributes }
        }.to change(CcReconciliation, :count).by(0)
      end

    
      it "renders a response with 422 status (i.e. to display the 'new' template)" do
        post cc_reconciliations_url, params: { cc_reconciliation: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested cc_reconciliation" do
        cc_reconciliation = CcReconciliation.create! valid_attributes
        patch cc_reconciliation_url(cc_reconciliation), params: { cc_reconciliation: new_attributes }
        cc_reconciliation.reload
        skip("Add assertions for updated state")
      end

      it "redirects to the cc_reconciliation" do
        cc_reconciliation = CcReconciliation.create! valid_attributes
        patch cc_reconciliation_url(cc_reconciliation), params: { cc_reconciliation: new_attributes }
        cc_reconciliation.reload
        expect(response).to redirect_to(cc_reconciliation_url(cc_reconciliation))
      end
    end

    context "with invalid parameters" do
    
      it "renders a response with 422 status (i.e. to display the 'edit' template)" do
        cc_reconciliation = CcReconciliation.create! valid_attributes
        patch cc_reconciliation_url(cc_reconciliation), params: { cc_reconciliation: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested cc_reconciliation" do
      cc_reconciliation = CcReconciliation.create! valid_attributes
      expect {
        delete cc_reconciliation_url(cc_reconciliation)
      }.to change(CcReconciliation, :count).by(-1)
    end

    it "redirects to the cc_reconciliations list" do
      cc_reconciliation = CcReconciliation.create! valid_attributes
      delete cc_reconciliation_url(cc_reconciliation)
      expect(response).to redirect_to(cc_reconciliations_url)
    end
  end
end
