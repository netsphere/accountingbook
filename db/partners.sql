-- 取引先マスタ
CREATE TABLE partners (
  id          serial PRIMARY KEY,
  book_id     VARCHAR(14)  REFERENCES books (id), -- NOT NULL

  name        VARCHAR(40) NOT NULL, -- 名前
  description VARCHAR(250) NOT NULL,  -- コメント

  active         boolean NOT NULL,

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,  -- 作成時刻
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP, -- 更新時刻
  update_user_code VARCHAR(40) REFERENCES users(login)
);

