

-- 請求書の明細行
CREATE TABLE invoice_details (
  id               serial PRIMARY KEY,

  -- 親
  invoice_id       int NOT NULL REFERENCES invoices (id),

  -- 行番号. 1始まり
  lineno           int NOT NULL,

  account_title_id int NOT NULL REFERENCES account_titles (id),
  
  amount           int NOT NULL,

  remarks          VARCHAR(200) NOT NULL,

  created_at       TIMESTAMP NOT NULL
);
