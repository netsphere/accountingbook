-- -*- coding:utf-8 -*-

-- 支払いの内訳 = 請求書の消し込み
CREATE TABLE payment_amounts (
  id         serial PRIMARY KEY,

  -- cash_slip_idまたはcc_transaction_idのいずれかを設定.
  -- 親 ... 現金払いのとき
  payment_id      int REFERENCES payments (id),

  -- 支払う経費
  invoice_id int NOT NULL REFERENCES invoices(id),
  
  amount     DECIMAL(12, 2) NOT NULL,

  UNIQUE (payment_id, invoice_id)
);
