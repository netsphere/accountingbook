
-- ユーザグループ
CREATE TABLE spaces (
  id         serial PRIMARY KEY,
  urlkey     VARCHAR(30) NOT NULL UNIQUE,
  
  name       VARCHAR(40) NOT NULL,

  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);
