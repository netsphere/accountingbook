
-- 勘定科目は, 木ではなく, 有向非巡回グラフ (DAG). 親も複数持てるようにする.
-- 帳票では, 表示並び順を決めるのみ.
-- <s>閉包テーブルにする。Closure tree.</s> NG. make a simple DAG.
CREATE TABLE account_title_edges (
  id        serial PRIMARY KEY,

  -- 自分自身 (distance = 0) は登録しない.
  -- Root は, 自分が child 側に出現しない, で識別する。
  parent_id int NOT NULL REFERENCES account_titles (id),
  child_id  int NOT NULL REFERENCES account_titles (id),

  -- 後続の migration にて, 次の列を追加;
  --   + main_parent
  --   + distance
  --   + sign_reversal
  
  created_at       TIMESTAMP NOT NULL,

  -- データ移行はナル値
  create_user_code VARCHAR(40) REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login),

  UNIQUE (parent_id, child_id)
);
