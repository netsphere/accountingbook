-- -*- coding:utf-8 -*-

-- 会計帳簿の設定
CREATE TABLE book_settings (
  book_id          VARCHAR(14) NOT NULL REFERENCES books (id) UNIQUE,

  -----------------------------------------
  -- 特殊な勘定科目
  
  unappropriated_item int REFERENCES account_titles(id), -- 未処分利益科目

  -- 未払金勘定
  --payable_item     int REFERENCES account_titles(id), 

  -- 初期残高 = この翌日から仕訳を入力できる。
  opening_date     DATE,      -- 記帳開始日（利用開始日ではなく、初期設定で任意に設定）
  fixed_date       DATE,      -- 締め日
  closing_day      int NOT NULL,  -- 月末日  ex. 25日

  theme_name       VARCHAR(40),  -- 画面テーマの名前
  entry_no_counter int NOT NULL, -- 伝票番号を生成

  -- 前回の操作を記憶しておく. TODO: ユーザの設定に移す？
  ledger_last_item int REFERENCES account_titles(id), -- 元帳の前回の勘定科目名
  cash_balance_last_item int REFERENCES account_titles (id), -- 残高をあわせる
  week_last_wday   int,        -- 週次推移表で、週末日
  
  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
);
