
-- 会計帳簿
CREATE TABLE books (
  -- @note ActiveRecord は id として文字列も可.
  --       URL の一部にするときは, 名前が `id` でないと大変
  id               VARCHAR(14) PRIMARY KEY,    

  -- 所有者は, 多:多でつなぐ.
  -- user_group_code  VARCHAR(30) NOT NULL REFERENCES spaces (urlkey),

  --urlkey           VARCHAR(30) NOT NULL UNIQUE,  -- 半角英数; URLの一部を構成

  name             VARCHAR(40) NOT NULL,         -- 日本語での名前
  description      VARCHAR(250) NOT NULL,        -- 説明

  licence_key      VARCHAR(64),
  
  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
);
