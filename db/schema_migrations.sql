
-- migration未済を管理
CREATE TABLE schema_migrations (
  "version" VARCHAR(255) NOT NULL UNIQUE
);
INSERT INTO schema_migrations (version) VALUES ('20120501000000');
INSERT INTO schema_migrations (version) VALUES ('20120514000000');
INSERT INTO schema_migrations (version) VALUES ('20120514101159');
INSERT INTO schema_migrations (version) VALUES ('20120523125122');
INSERT INTO schema_migrations (version) VALUES ('20120627140936');
INSERT INTO schema_migrations (version) VALUES ('20120723135120');
INSERT INTO schema_migrations (version) VALUES ('20130907000824'); -- ac_tree
INSERT INTO schema_migrations (version) VALUES ('20130929220851'); -- cc mast.
INSERT INTO schema_migrations (version) VALUES ('20131003120127'); -- cc_tran
INSERT INTO schema_migrations (version) VALUES ('20131006222616'); -- inv_detail
INSERT INTO schema_migrations (version) VALUES ('20150717102445'); -- ac_kind

INSERT INTO schema_migrations (version) VALUES ('20171010103719');
INSERT INTO schema_migrations (version) VALUES ('20171105124302');
INSERT INTO schema_migrations (version) VALUES ('20180621101602');
INSERT INTO schema_migrations (version) VALUES ('20190128124750');

