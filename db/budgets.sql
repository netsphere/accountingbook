-- -*- coding:utf-8 -*-
-- 予算　ある日付について、勘定科目と金額の組
CREATE TABLE budgets (
  id      serial PRIMARY KEY,
  book_id VARCHAR(14) NOT NULL REFERENCES books (id),

  date    DATE NOT NULL,
  comment VARCHAR(250) NOT NULL, -- 内容

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,  -- 作成時刻
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP, -- 更新時刻
  update_user_code VARCHAR(40) REFERENCES users(login)
);
