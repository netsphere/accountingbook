
-- 領収書による経費精算。小切手 / credit card の取引
-- CSVファイルをインポートする
CREATE TABLE expenses  (
  id               serial PRIMARY KEY,
  book_id          VARCHAR(14) NOT NULL REFERENCES books (id),

  -- クレジットカード
  cash_account_id   int NOT NULL REFERENCES cash_accounts (id),

  due_date         DATE NOT NULL,

  -- 取引日 (利用日)
  date             DATE NOT NULL,

  -- 利用店名
  shop_name        VARCHAR(250) NOT NULL,

  -- 1 のとき家族カード
  family_flag      int NOT NULL,

  -- ex) "1回"
  payment_type     int NOT NULL,

  -- 合計 ...引き落とし時値引きがあることがある => 明細行を作る
  -- 明細行は payment_amounts
  amount           DECIMAL(12, 2) NOT NULL,

  -- 備考
  remarks          VARCHAR(250) NOT NULL,

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
);
