# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2024_11_28_072710) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ac_ovrds", force: :cascade do |t|
    t.integer "account_title_id", null: false
    t.string "book_id", limit: 14, null: false
    t.boolean "active", null: false
    t.string "name", null: false
    t.string "name_short", null: false
    t.integer "sign", limit: 2, null: false, comment: "元帳の表示正残. 借方 = +1"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_title_id", "book_id"], name: "index_ac_ovrds_on_account_title_id_and_book_id", unique: true
    t.index ["account_title_id"], name: "index_ac_ovrds_on_account_title_id"
  end

  create_table "account_title_edges", id: :serial, force: :cascade do |t|
    t.integer "parent_id", null: false
    t.integer "child_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
    t.boolean "main_parent", null: false
    t.integer "weight", null: false

    t.unique_constraint ["parent_id", "child_id"], name: "account_title_edges_parent_id_child_id_key"
  end

  create_table "account_titles", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14
    t.boolean "aggregate", null: false
    t.integer "ac_type", limit: 2, null: false
    t.integer "kind", limit: 2, null: false
    t.string "sort", limit: 8, null: false
    t.string "name", limit: 40, null: false
    t.string "name_short", limit: 20
    t.string "description", limit: 250, null: false
    t.integer "sign", limit: 2, null: false
    t.date "end_day"
    t.boolean "enable_invoice", null: false
    t.integer "payable_id"
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
    t.integer "tax_group", limit: 2, null: false

    t.unique_constraint ["book_id", "sort"], name: "account_titles_book_id_sort_key"
  end

  create_table "book_settings", id: false, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.integer "unappropriated_item"
    t.date "opening_date"
    t.date "fixed_date"
    t.integer "closing_day", null: false
    t.string "theme_name", limit: 40
    t.integer "entry_no_counter", null: false
    t.integer "ledger_last_item"
    t.integer "cash_balance_last_item"
    t.integer "week_last_wday"
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40

    t.unique_constraint ["book_id"], name: "book_settings_book_id_key"
  end

  create_table "books", id: { type: :string, limit: 14 }, force: :cascade do |t|
    t.string "name", limit: 40, null: false
    t.string "description", limit: 250, null: false
    t.string "licence_key", limit: 64
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
  end

  create_table "books_users", force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.integer "user_id", null: false
    t.integer "member_type", limit: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["book_id", "user_id"], name: "index_books_users_on_book_id_and_user_id", unique: true
    t.index ["user_id"], name: "index_books_users_on_user_id"
  end

  create_table "bp_addresses", force: :cascade do |t|
    t.integer "partner_id", null: false
    t.string "book_id", limit: 14, null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_id"], name: "index_bp_addresses_on_partner_id"
  end

  create_table "budget_details", id: :serial, force: :cascade do |t|
    t.integer "budget_id", null: false
    t.integer "account_title_id", null: false
    t.integer "amount", null: false

    t.unique_constraint ["budget_id", "account_title_id"], name: "budget_details_budget_id_account_title_id_key"
  end

  create_table "budgets", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.date "date", null: false
    t.string "comment", limit: 250, null: false
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
  end

  create_table "cash_accounts", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.string "sys_name", limit: 40
    t.string "name", limit: 40, null: false
    t.string "description", limit: 250, null: false
    t.integer "account_title_id", null: false
    t.boolean "active", null: false
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40

    t.unique_constraint ["sys_name"], name: "cash_accounts_sys_name_key"
  end

  create_table "cc_reconc_details", id: :serial, force: :cascade do |t|
    t.integer "cc_reconciliation_id", null: false
    t.integer "expense_id", null: false
    t.decimal "amount", precision: 12, scale: 2, null: false

    t.unique_constraint ["cc_reconciliation_id", "expense_id"], name: "cc_reconc_details_cc_reconciliation_id_expense_id_key"
  end

  create_table "cc_reconciliations", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.date "date", null: false
    t.integer "cash_account_id", null: false
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
  end

  create_table "div_ovrds", force: :cascade do |t|
    t.integer "division_id", null: false
    t.string "book_id", limit: 14, null: false
    t.date "end_day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["division_id", "book_id"], name: "index_div_ovrds_on_division_id_and_book_id", unique: true
    t.index ["division_id"], name: "index_div_ovrds_on_division_id"
  end

  create_table "division_edges", force: :cascade do |t|
    t.integer "parent_id", null: false
    t.integer "child_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "main_parent", null: false
    t.index ["child_id"], name: "index_division_edges_on_child_id"
    t.index ["parent_id", "child_id"], name: "index_division_edges_on_parent_id_and_child_id", unique: true
    t.index ["parent_id"], name: "index_division_edges_on_parent_id"
  end

  create_table "divisions", force: :cascade do |t|
    t.string "book_id", limit: 14
    t.string "division_code", limit: 12, null: false
    t.string "name", limit: 40, null: false
    t.boolean "purchase_active", null: false
    t.boolean "aggregate", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "vat_disable", null: false, comment: "trueの部門では, 税コードn/a, 消費税額 0.00にする"
    t.string "description", null: false
    t.index ["division_code", "book_id"], name: "index_divisions_on_division_code_and_book_id", unique: true
  end

  create_table "expenses", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.integer "cash_account_id", null: false
    t.date "due_date", null: false
    t.date "date", null: false
    t.string "shop_name", limit: 250, null: false
    t.integer "family_flag", null: false
    t.integer "payment_type", null: false
    t.decimal "amount", precision: 12, scale: 2, null: false
    t.string "remarks", limit: 250, null: false
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
  end

  create_table "invoice_details", id: :serial, force: :cascade do |t|
    t.integer "invoice_id", null: false
    t.integer "lineno", null: false
    t.integer "account_title_id", null: false
    t.integer "amount", null: false
    t.string "remarks", limit: 200, null: false
    t.datetime "created_at", precision: nil, null: false
  end

  create_table "invoices", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.integer "partner_id", null: false
    t.date "date", null: false
    t.integer "amount_total", null: false
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
  end

  create_table "journal_entries", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.date "date", null: false
    t.integer "entry_no", null: false
    t.integer "account_id", null: false
    t.integer "partner_id"
    t.decimal "amount", precision: 12, scale: 2, null: false
    t.string "comment", limit: 250, null: false
    t.integer "invoice_id"
    t.integer "payment_amount_id"
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
    t.integer "dr_or_cr", limit: 2, null: false, comment: "借方に表示 = +1, 貸方に表示 = -1"
    t.integer "contra_ac_id"
    t.integer "division_id", null: false
    t.integer "vat_id"
    t.decimal "vat_amt", precision: 12, scale: 2, null: false
    t.date "due_date"
    t.integer "vat_excluded", limit: 2, null: false, comment: "period_ends を参照しなくてよいようにする。0:免税 1:税込経理 2:税抜経理"
    t.string "status", limit: 1, null: false, comment: "未照合='O', 照合済み='C'"
    t.index ["contra_ac_id"], name: "index_journal_entries_on_contra_ac_id"
    t.index ["division_id"], name: "index_journal_entries_on_division_id"
    t.index ["vat_id"], name: "index_journal_entries_on_vat_id"
  end

  create_table "partner_ovrds", force: :cascade do |t|
    t.integer "partner_id", null: false
    t.string "book_id", limit: 14, null: false
    t.string "remarks", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_id"], name: "index_partner_ovrds_on_partner_id"
  end

  create_table "partners", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14
    t.string "name", limit: 40, null: false
    t.string "description", limit: 250, null: false
    t.boolean "active", null: false
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
    t.string "kana_name", null: false
    t.string "country", limit: 2, null: false
    t.string "hq_addr", null: false
    t.string "trn"
    t.index ["country", "trn"], name: "index_partners_on_country_and_trn", unique: true
  end

  create_table "payment_amounts", id: :serial, force: :cascade do |t|
    t.integer "payment_id"
    t.integer "invoice_id", null: false
    t.decimal "amount", precision: 12, scale: 2, null: false

    t.unique_constraint ["payment_id", "invoice_id"], name: "payment_amounts_payment_id_invoice_id_key"
  end

  create_table "payments", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.date "date", null: false
    t.integer "cash_account_id", null: false
    t.integer "lock_version", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
  end

  create_table "period_ends", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14, null: false
    t.date "date", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "create_user_code", limit: 40, null: false
    t.datetime "updated_at", precision: nil
    t.string "update_user_code", limit: 40
    t.integer "vat_taxable", limit: 2, null: false
    t.integer "vat_exclusive", limit: 2, null: false, comment: "0 免税・税込経理, 1 税抜経理"
    t.date "registration_of_taxable"
    t.string "description", null: false

    t.unique_constraint ["book_id", "date"], name: "period_ends_book_id_date_key"
  end

  create_table "reporting_titles", id: :serial, force: :cascade do |t|
    t.integer "report_id", null: false
    t.integer "position", null: false
    t.integer "account_title_id", null: false
    t.integer "sign", limit: 2, null: false
    t.integer "ac_type", limit: 2, null: false, comment: "1:残高, 4:発生額"

    t.unique_constraint ["report_id", "position"], name: "reporting_titles_report_id_position_key"
  end

  create_table "reports", id: :serial, force: :cascade do |t|
    t.string "book_id", limit: 14
    t.string "name", limit: 40, null: false
    t.string "description", limit: 250, null: false
  end

  create_table "spaces", id: :serial, force: :cascade do |t|
    t.string "urlkey", limit: 30, null: false
    t.string "name", limit: 40, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil

    t.unique_constraint ["urlkey"], name: "spaces_urlkey_key"
  end

  create_table "spaces_users", id: :serial, force: :cascade do |t|
    t.integer "space_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil

    t.unique_constraint ["user_id", "space_id"], name: "spaces_users_user_id_space_id_key"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "login", limit: 40, null: false
    t.string "email", limit: 40, null: false
    t.string "crypted_password", limit: 64
    t.string "salt", limit: 64
    t.string "name", limit: 40, null: false
    t.datetime "expire", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil
    t.boolean "admin_role", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.unique_constraint ["login"], name: "users_login_key"
  end

  create_table "vats", force: :cascade do |t|
    t.boolean "active", null: false
    t.string "country", limit: 2, null: false
    t.integer "tax_group", limit: 2, null: false
    t.string "tax_code", limit: 8, null: false
    t.string "name", limit: 20, null: false
    t.date "effective_day", null: false
    t.decimal "tax_rate", precision: 4, scale: 2, null: false
    t.string "description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tax_code"], name: "index_vats_on_tax_code", unique: true
  end

  add_foreign_key "ac_ovrds", "account_titles"
  add_foreign_key "ac_ovrds", "books", name: "ac_ovrds_book_id_fkey"
  add_foreign_key "account_title_edges", "account_titles", column: "child_id", name: "account_title_edges_child_id_fkey"
  add_foreign_key "account_title_edges", "account_titles", column: "parent_id", name: "account_title_edges_parent_id_fkey"
  add_foreign_key "account_title_edges", "users", column: "create_user_code", primary_key: "login", name: "account_title_edges_create_user_code_fkey"
  add_foreign_key "account_title_edges", "users", column: "update_user_code", primary_key: "login", name: "account_title_edges_update_user_code_fkey"
  add_foreign_key "account_titles", "account_titles", column: "payable_id", name: "account_titles_payable_id_fkey"
  add_foreign_key "account_titles", "books", name: "account_titles_book_id_fkey"
  add_foreign_key "account_titles", "users", column: "create_user_code", primary_key: "login", name: "account_titles_create_user_code_fkey"
  add_foreign_key "account_titles", "users", column: "update_user_code", primary_key: "login", name: "account_titles_update_user_code_fkey"
  add_foreign_key "book_settings", "account_titles", column: "cash_balance_last_item", name: "book_settings_cash_balance_last_item_fkey"
  add_foreign_key "book_settings", "account_titles", column: "ledger_last_item", name: "book_settings_ledger_last_item_fkey"
  add_foreign_key "book_settings", "account_titles", column: "unappropriated_item", name: "book_settings_unappropriated_item_fkey"
  add_foreign_key "book_settings", "books", name: "book_settings_book_id_fkey"
  add_foreign_key "book_settings", "users", column: "create_user_code", primary_key: "login", name: "book_settings_create_user_code_fkey"
  add_foreign_key "book_settings", "users", column: "update_user_code", primary_key: "login", name: "book_settings_update_user_code_fkey"
  add_foreign_key "books", "users", column: "create_user_code", primary_key: "login", name: "books_create_user_code_fkey"
  add_foreign_key "books", "users", column: "update_user_code", primary_key: "login", name: "books_update_user_code_fkey"
  add_foreign_key "books_users", "books", name: "books_users_book_id_fkey"
  add_foreign_key "books_users", "users"
  add_foreign_key "bp_addresses", "books", name: "bp_addresses_book_id_fkey"
  add_foreign_key "bp_addresses", "partners"
  add_foreign_key "budget_details", "account_titles", name: "budget_details_account_title_id_fkey"
  add_foreign_key "budget_details", "budgets", name: "budget_details_budget_id_fkey"
  add_foreign_key "budgets", "books", name: "budgets_book_id_fkey"
  add_foreign_key "budgets", "users", column: "create_user_code", primary_key: "login", name: "budgets_create_user_code_fkey"
  add_foreign_key "budgets", "users", column: "update_user_code", primary_key: "login", name: "budgets_update_user_code_fkey"
  add_foreign_key "cash_accounts", "account_titles", name: "cash_accounts_account_title_id_fkey"
  add_foreign_key "cash_accounts", "books", name: "cash_accounts_book_id_fkey"
  add_foreign_key "cash_accounts", "users", column: "create_user_code", primary_key: "login", name: "cash_accounts_create_user_code_fkey"
  add_foreign_key "cash_accounts", "users", column: "update_user_code", primary_key: "login", name: "cash_accounts_update_user_code_fkey"
  add_foreign_key "cc_reconc_details", "cc_reconciliations", name: "cc_reconc_details_cc_reconciliation_id_fkey"
  add_foreign_key "cc_reconc_details", "expenses", name: "cc_reconc_details_expense_id_fkey"
  add_foreign_key "cc_reconciliations", "books", name: "cc_reconciliations_book_id_fkey"
  add_foreign_key "cc_reconciliations", "cash_accounts", name: "cc_reconciliations_cash_account_id_fkey"
  add_foreign_key "cc_reconciliations", "users", column: "create_user_code", primary_key: "login", name: "cc_reconciliations_create_user_code_fkey"
  add_foreign_key "cc_reconciliations", "users", column: "update_user_code", primary_key: "login", name: "cc_reconciliations_update_user_code_fkey"
  add_foreign_key "div_ovrds", "books", name: "div_ovrds_book_id_fkey"
  add_foreign_key "division_edges", "divisions", column: "child_id"
  add_foreign_key "division_edges", "divisions", column: "parent_id"
  add_foreign_key "divisions", "books", name: "divisions_book_id_fkey"
  add_foreign_key "expenses", "books", name: "expenses_book_id_fkey"
  add_foreign_key "expenses", "cash_accounts", name: "expenses_cash_account_id_fkey"
  add_foreign_key "expenses", "users", column: "create_user_code", primary_key: "login", name: "expenses_create_user_code_fkey"
  add_foreign_key "expenses", "users", column: "update_user_code", primary_key: "login", name: "expenses_update_user_code_fkey"
  add_foreign_key "invoice_details", "account_titles", name: "invoice_details_account_title_id_fkey"
  add_foreign_key "invoice_details", "invoices", name: "invoice_details_invoice_id_fkey"
  add_foreign_key "invoices", "books", name: "invoices_book_id_fkey"
  add_foreign_key "invoices", "partners", name: "invoices_partner_id_fkey"
  add_foreign_key "invoices", "users", column: "create_user_code", primary_key: "login", name: "invoices_create_user_code_fkey"
  add_foreign_key "invoices", "users", column: "update_user_code", primary_key: "login", name: "invoices_update_user_code_fkey"
  add_foreign_key "journal_entries", "account_titles", column: "account_id", name: "journal_entries_dr_account_id_fkey"
  add_foreign_key "journal_entries", "account_titles", column: "contra_ac_id"
  add_foreign_key "journal_entries", "books", name: "journal_entries_book_id_fkey"
  add_foreign_key "journal_entries", "divisions"
  add_foreign_key "journal_entries", "invoices", name: "journal_entries_invoice_id_fkey"
  add_foreign_key "journal_entries", "partners", name: "journal_entries_dr_partner_id_fkey"
  add_foreign_key "journal_entries", "payment_amounts", name: "journal_entries_payment_amount_id_fkey"
  add_foreign_key "journal_entries", "users", column: "create_user_code", primary_key: "login", name: "journal_entries_create_user_code_fkey"
  add_foreign_key "journal_entries", "users", column: "update_user_code", primary_key: "login", name: "journal_entries_update_user_code_fkey"
  add_foreign_key "journal_entries", "vats"
  add_foreign_key "partner_ovrds", "books", name: "partner_ovrds_book_id_fkey"
  add_foreign_key "partner_ovrds", "partners"
  add_foreign_key "partners", "books", name: "partners_book_id_fkey"
  add_foreign_key "partners", "users", column: "create_user_code", primary_key: "login", name: "partners_create_user_code_fkey"
  add_foreign_key "partners", "users", column: "update_user_code", primary_key: "login", name: "partners_update_user_code_fkey"
  add_foreign_key "payment_amounts", "invoices", name: "payment_amounts_invoice_id_fkey"
  add_foreign_key "payment_amounts", "payments", name: "payment_amounts_payment_id_fkey"
  add_foreign_key "payments", "books", name: "payments_book_id_fkey"
  add_foreign_key "payments", "cash_accounts", name: "payments_cash_account_id_fkey"
  add_foreign_key "payments", "users", column: "create_user_code", primary_key: "login", name: "payments_create_user_code_fkey"
  add_foreign_key "payments", "users", column: "update_user_code", primary_key: "login", name: "payments_update_user_code_fkey"
  add_foreign_key "period_ends", "books", name: "period_ends_book_id_fkey"
  add_foreign_key "period_ends", "users", column: "create_user_code", primary_key: "login", name: "period_ends_create_user_code_fkey"
  add_foreign_key "period_ends", "users", column: "update_user_code", primary_key: "login", name: "period_ends_update_user_code_fkey"
  add_foreign_key "reporting_titles", "account_titles", name: "reporting_titles_account_title_id_fkey"
  add_foreign_key "reporting_titles", "reports", name: "reporting_titles_report_id_fkey"
  add_foreign_key "reports", "books", name: "reports_book_id_fkey"
  add_foreign_key "spaces_users", "spaces", name: "spaces_users_space_id_fkey"
  add_foreign_key "spaces_users", "users", name: "spaces_users_user_id_fkey"
end
