
-- 報告様式 - 勘定科目の表示順
CREATE TABLE reporting_titles (
  id               serial PRIMARY KEY,
  -- 親
  report_id        int NOT NULL REFERENCES reports(id),

  -- For acts_as_list gem.
  -- acts_as_list は 2024年に v1.2.2. 現役.
  -- 同じ作者による ranked-model が acts_as_list の後継.
  position         int NOT NULL,   
  account_title_id int NOT NULL REFERENCES account_titles(id),

  -- 科目は複数回, 表示可.
  UNIQUE (report_id, position)
);
