
-- 現金、銀行口座、クレジットカードマスタ
CREATE TABLE cash_accounts  (
  id               serial PRIMARY KEY,
  book_id          VARCHAR(14) NOT NULL REFERENCES books (id),

  -- 請求書に記載のカードブランド名. 
  -- インポートのとり違いを抑制する. ユーザは変更不可
  sys_name         VARCHAR(40) UNIQUE,

  -- ユーザが付ける名前
  name             VARCHAR(40) NOT NULL,

  description      VARCHAR(250) NOT NULL,

  account_title_id int NOT NULL REFERENCES account_titles (id),

  active           boolean NOT NULL,

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
);
