
-- 報告様式 (テンプレート). ex. 試算表、資金繰り表
CREATE TABLE reports (
  id       serial PRIMARY KEY,
  
  -- 親. ナル値はシステム帳票
  book_id  VARCHAR(14) REFERENCES books (id),
      
  name     VARCHAR(40) NOT NULL,
  description VARCHAR(250) NOT NULL -- 説明
);
