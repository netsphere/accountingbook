
CREATE TABLE spaces_users (
  id         serial PRIMARY KEY,
  
  space_id   int NOT NULL REFERENCES spaces (id),
  user_id    int NOT NULL REFERENCES users (id),

  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP,

  UNIQUE (user_id, space_id)
);

  
