-- -*- coding:utf-8 -*-

-- 支払い  リボ払いがあることに注意。 
CREATE TABLE cc_reconciliations (
  id          serial PRIMARY KEY,
  book_id     VARCHAR(14) NOT NULL REFERENCES books (id),
  
  date     DATE NOT NULL,     -- 支払日 = 伝票日付

  -- 支払い元銀行コード
  cash_account_id  int NOT NULL REFERENCES cash_accounts (id),
  
  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,  -- 作成時刻
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP, -- 更新時刻
  update_user_code VARCHAR(40) REFERENCES users(login)
);
