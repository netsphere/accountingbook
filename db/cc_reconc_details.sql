
-- クレジットカードの引き落とし, 明細行
CREATE TABLE cc_reconc_details (
  id           serial PRIMARY KEY,

  -- 親
  cc_reconciliation_id      int NOT NULL REFERENCES cc_reconciliations (id),

  -- 消し込む経費
  expense_id        int NOT NULL REFERENCES expenses (id),

  -- 分割払いがある
  amount            DECIMAL(12, 2) NOT NULL,

  UNIQUE (cc_reconciliation_id, expense_id)
);
