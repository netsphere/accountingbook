
-- 会計期間 (の期末日)
CREATE TABLE period_ends (
  id      serial PRIMARY KEY,
  book_id VARCHAR(14) NOT NULL REFERENCES books (id),

  -- 期末日
  date    DATE NOT NULL,

  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login),

  UNIQUE (book_id, date)
);
