-- -*- coding:utf-8 -*-
CREATE TABLE budget_details (
  id               serial PRIMARY KEY,
  budget_id        int NOT NULL REFERENCES budgets(id),

  account_title_id int NOT NULL REFERENCES account_titles(id),
  -- 金額。符号は, 常に借方がプラス、貸方がマイナス。
  -- (勘定科目の区分を変更しても利益が変わらないように。)
  amount           int NOT NULL,

  UNIQUE (budget_id, account_title_id)
);
