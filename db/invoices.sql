-- -*- coding:utf-8 -*-

-- 請求書 (AP invoice) 
CREATE TABLE invoices (
  id         serial PRIMARY KEY,
  book_id    VARCHAR(14) NOT NULL REFERENCES books (id),

  partner_id int NOT NULL REFERENCES partners(id),  -- 取引先

  date       DATE NOT NULL,   -- 購入日または締め日。伝票日付ではない。
  amount_total int NOT NULL,  -- 未払金額のキャッシュ。整合性はプログラムで確保。  

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
);

