
=begin
勘定科目には 00 対象外, 10 課税売上, 50 課税仕入を設定

◆対象外  outside scope of tax          00   n/a

◆Output VAT
 非課税売上 VAT exemption              30    0   1989.4.1
     有価証券の非課税売上  -- 勘定科目で区別

◆Input VAT
          VAT exemptions for small enterprises
          -> 控除対象外消費税になるのではない。対価の額で調整
 非課税仕入  -- 対象外と区別しない
 Reverse Charge -- 本則課税で課税売上割合95%未満のとき.
                                       80    0.0% (外10%)

 貸倒損失                 -- 勘定科目で区別するか. 売上 side にする
 固定資産の控除対象外消費税 -- 同じく勘定科目で.
 交際費
=end


# 消費税を追加する。結構複雑 (制度が複雑)
class CreateVats < ActiveRecord::Migration[7.1]
  def change
    # 税率は、開始日とペアにする。ガンガン変更される
    # グローバルに持つ。
    create_table :vats do |t|
      t.boolean :active, null:false

      # `NOT NULL` なので, 対象外が作れない
      t.column :country, 'CHAR(2) NOT NULL'

      # 税グループ. 国で区別しない
      t.column :tax_group, "SMALLINT NOT NULL"
      
      # 税率が変わるときにも新たに別コードにすること
      # × "OOS_00" アカン
      t.string :tax_code, limit:8, null:false, index:{unique:true}
      
      # "課税売10%", "非課売", ...
      t.string 'name', limit:20, null:false

      # 開始日
      t.date :effective_day, null:false

      # 税率. 後から変更不可。新たにレコード作ること
      # `NUMERIC` は厳密に指定精度, `DECIMAL` は指定以上の精度、通常は後者を使え
      t.column :tax_rate, 'DECIMAL(4, 2) NOT NULL'

      t.string 'description', null:false
      
      t.timestamps
    end

    # 仕訳に追加する. 対象外があるため、ナル可
    # 税抜の足し上げを簡単にするため、税額フィールドもつくる。
    change_table "journal_entries" do |t|
      t.references :vat, type: :integer, foreign_key:true
      # `amount` と型を合わせる
      t.column     :vat_amt, 'DECIMAL(12,2) NOT NULL'
    end
    
    # 年度ごとの設定
    # 本則、簡易課税、免税業者。
    # 本則課税の課税事業者は, 免税業者からの仕入れで税額控除できない。
    change_table "period_ends" do |t|
      # - 「消費税簡易課税制度選択届出書」を提出している場合でも、
      #   基準期間の課税売上高が 5,000 万円を超える場合には、本則課税
      # - 基準期間の課税売上高が1,000万円以下であっても,
      #   「消費税課税事業者選択届出書」を提出すれば、任意で課税事業者になれる
      # 年度ごとに都度選択して!
      # 0 免税事業者
      # 課税事業者: 1 課税(本則課税)  2 課税(簡易課税)  3 小規模事業者の2割特例
      t.column :vat_taxable, "SMALLINT NOT NULL"

      # この選択は独立. 普通は、簡易課税なら "税込経理" を選べ.
      # inclusive or exclusive of value added tax (VAT)
      t.column :vat_exclusive, "SMALLINT NOT NULL", comment: '0 免税・税込経理, 1 税抜経理'

      # 免税事業者 (個人・法人とも) がインボイス制度の登録をおこなった場合のみ,
      # 期中から課税事業者になることがある。
      #  - 免税の期間の仕訳を全部修正し、対象外にする方法は、大変。
      # See https://www.nta.go.jp/taxes/shiraberu/zeimokubetsu/shohi/keigenzeiritsu/pdf/qa/01-04.pdf
      #     問7. 問8 は個人のみ。
      t.date :registration_of_taxable
      
      # 理由メモ
      t.string 'description', null:false
    end

    change_table 'divisions' do |t|
      t.boolean :vat_disable, null:false, comment: 'trueの部門では, 税コードn/a, 消費税額 0.00にする'
    end

    # 消費税不可 = 0 (NOT NULL)
    change_table 'account_titles' do |t|
      t.column :tax_group, "SMALLINT NOT NULL"
    end
  end
end
