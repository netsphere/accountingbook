# -*- coding: utf-8 -*- 

class AccountTitlesAddKind < ActiveRecord::Migration
  def self.up
    execute <<EOF
ALTER TABLE account_titles ADD COLUMN kind           int;
ALTER TABLE account_titles ADD COLUMN enable_invoice boolean;
ALTER TABLE account_titles ADD COLUMN payable_id     int REFERENCES account_titles(id);
EOF

    Book.all.each do |book|
      AccountTitle.where("book_id = ?", book.id).each do |ac|
        if ac.id == book.setting.payable_item
          # 未払金a/c
          ac.kind = 2
          ac.enable_invoice = false
        elsif ac.cf_type == 4 # 現金同等物
          ac.kind = 4
          ac.enable_invoice = false
        elsif ac.ac_type == 5 || ac.ac_type == 1 # 営業費用・資産
          ac.kind = 0
          ac.enable_invoice = true
          ac.payable_id = book.setting.payable_item
        else
          # 負債、純資産、営業収益、その他の収益、その他の費用
          ac.kind = 0
          ac.enable_invoice = false
        end
        ac.ac_type = 1 if ac.ac_type == 2 || ac.ac_type == 3
        ac.ac_type = 4 if ac.ac_type >= 5 && ac.ac_type <= 7
        ac.save!
      end
    end

    execute <<EOF
ALTER TABLE account_titles ALTER kind           SET NOT NULL;
ALTER TABLE account_titles ALTER enable_invoice SET NOT NULL;
EOF

    # cf_type は、もはや不要
    execute <<EOF
ALTER TABLE account_titles DROP COLUMN cf_type;
ALTER TABLE book_settings DROP COLUMN payable_item;
EOF
  end
end
