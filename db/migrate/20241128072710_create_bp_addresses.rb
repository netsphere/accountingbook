
class CreateBpAddresses < ActiveRecord::Migration[7.2]
  def change
    create_table :bp_addresses do |t|
      t.references :partner, type: :integer, null:false, foreign_key:true
      t.column :book_id, 'VARCHAR(14) NOT NULL REFERENCES books(id)'

      t.string :name, null:false
      
      t.timestamps
    end
  end
end
