# -*- coding:utf-8 -*-


# マスタテーブルにフィールドを追加。
class MasterMoreAttributes < ActiveRecord::Migration
  class << self
    def up
      execute "ALTER TABLE partners ADD COLUMN active boolean NOT NULL DEFAULT TRUE"
      execute "ALTER TABLE partners ALTER COLUMN active DROP DEFAULT"
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end  # << self
end
