
=begin
    ●部門の足し上げをどうするか?
      - 勘定科目のように tree か DAG にするか
      - 1レベルに限定して, セットを複数作るか
            --> 2レベルはほしい。root/事業/店舗
=end


# 部門を追加する
# 簡易課税の事業区分は、名前に反して事業の区分ではないので、勘定科目で区別すべき
#     -> 業に関わらず固定資産の売却収入は第4、など
# 所得区分 (事業所得, 不動産所得, ...) は部門で区別すべき
class CreateDivisions < ActiveRecord::Migration[7.1]
  def change
    create_table :divisions do |t|
      # ナル値のときは system item. ユーザは変更不可
      t.column :book_id, 'VARCHAR(14) REFERENCES books(id)'
      
      t.string :division_code, limit: 12, null:false

      t.string :name, limit:40, null:false

      t.boolean :purchase_active, null:false

      # Tree にする
      t.boolean :aggregate, null:false
      
      t.timestamps
    end
    add_index :divisions, [:division_code, :book_id], unique:true

    create_table :div_ovrds do |t|
      t.references :division, type: :integer, null:false
      t.column :book_id, 'VARCHAR(14) NOT NULL REFERENCES books(id)'
      
      # 終了日. ナル値は active
      t.date   :end_day

      t.timestamps
    end
    add_index :div_ovrds, [:division_id, :book_id], unique:true

    # <s>親子は closure tree にて。Table 名は `_hierarchies` で決め打ち。</s>
    create_table :division_edges do |t|
      t.references :parent, type: :integer, null:false,
                              foreign_key:{to_table: :divisions}
      t.references :child, type: :integer, null:false,
                                foreign_key:{to_table: :divisions}
      #t.integer :distance, null:false
      
      t.timestamps
    end
    add_index :division_edges, [:parent_id, :child_id], unique:true
    
    # 仕訳に部門コードを追加する. 必須
    change_table "journal_entries" do |t|
      t.references :division, type: :integer, null:false, foreign_key:true
    end

  end
  
end
