# -*- coding:utf-8 -*-

require_relative "accounts_user"


# netsphere-accounts にユーザを追加する。
class MoveUsersToAccounts < ActiveRecord::Migration

  def self.create_users_on_accounts_db
    ::User.all.each do |user|
      a_user = Accounts::User.find_by_book_user user
      if a_user
        if user.login != a_user.login
          # メールアドレスでログインできるようにしておく。
          a_email = Accounts::Email.find_by_email_addr user.email
          if !a_email
            a_email = Accounts::Email.new :email_addr => user.email,
                                :user_id => a_user.id,
                                :can_login => true
            a_email.save!
          elsif !a_email.can_login
            a_email.can_login = true
            a_email.save!
          end
        end
      else
        a_user = Accounts::User.new :login => user.login,
                                   :active => true,
                                   :name => user.name,
                                   #:language => "ja",
                                   :timezone => "",
                                   :description => "",
                                   :crypted_password => user.crypted_password2,
                                   :salt => user.salt,
                                   :superuser => false
        a_user.save!
        a_e = Accounts::Email.new :email_addr => user.email,
                                    :user_id => a_user.id,
                                    :can_login => true
        a_e.save!
      end
    end # each
  end


  def self.up
    Accounts::User.transaction do
      create_users_on_accounts_db
    end
  end

end
