
# 会計帳簿の所有者
class CreateBooksUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :books_users do |t|
      t.column "book_id", "VARCHAR(14) NOT NULL REFERENCES books(id)"
      t.references :user, type: :integer, null:false, foreign_key:true
      t.column :member_type, "SMALLINT NOT NULL"
      
      t.timestamps
    end
    add_index :books_users, [:book_id, :user_id], unique:true
  end
end
