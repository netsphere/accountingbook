
class AddAdminRoleToUser < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :admin_role, :boolean, null:false
  end
end
