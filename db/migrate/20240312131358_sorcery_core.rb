

class SorceryCore < ActiveRecord::Migration[7.1]
  def up
    remove_column :users, :crypted_password
    rename_column :users, :crypted_password2, :crypted_password
    add_index :users, :email, unique:true
  end

  def down
    raise 
  end
end
