# -*- coding:utf-8 -*-

# 会計年度 (の期末日)
class CreatePeriodEnds < ActiveRecord::Migration
  def self.up
    execute <<EOF
CREATE TABLE period_ends (
  id      serial PRIMARY KEY,
  book_id int NOT NULL REFERENCES books (id),

  date    DATE NOT NULL,

  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login),

  UNIQUE (book_id, date)
);
EOF
  end
end
