# -*- coding:utf-8 -*-

# 仕訳の各行に, 取引先コードを追加する
class JournalEntries2Lines < ActiveRecord::Migration
  def up
    # テーブルの変更
    cols = ["RENAME dr_code TO dr_account_id",
            "ADD COLUMN dr_partner_id int REFERENCES partners (id)",
            "RENAME cr_code TO cr_account_id",
            "ADD COLUMN cr_partner_id int REFERENCES partners (id)"]

    execute( (cols.map do |col| 
                "ALTER TABLE journal_entries " + col end).join(";") )

    # 請求書: フィールドを補う
    JournalEntry.where("invoice_id IS NOT NULL").each do |r|
      r.cr_partner_id = r.invoice.partner_id
      r.save!
    end

=begin
    execute <<EOF
ALTER TABLE journal_entries DROP COLUMN cr_code;
ALTER TABLE journal_entries RENAME dr_code TO account_title_id
EOF
=end
  end


  def down
    raise ActiveRecord::IrreversibleMigration, "cannot down"
  end
end
