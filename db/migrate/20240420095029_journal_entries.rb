
# 仕訳を整える
class JournalEntries < ActiveRecord::Migration[7.1]
  def change
    # 仕訳の1行
    change_table :journal_entries do |t|
      # 1行に貸借を書くスタイルから、借方か貸方のどちらかを書くスタイルに.
      t.rename :dr_account_id, :account_id
      t.remove :cr_account_id

      t.rename :dr_partner_id, :partner_id
      t.remove :cr_partner_id

      # 金額はプラスが借方、マイナスが貸方で固定。
      # 金額表示を借方・貸方のどちらにするか. "CR" なら符号反転して貸方に表示
      t.column :dr_or_cr, "SMALLINT NOT NULL", comment: "借方に表示 = +1, 貸方に表示 = -1"

      # 相手勘定. 諸口は NULL
      # 32bit int にする.
      t.references :contra_ac, type: :integer, foreign_key:{to_table: :account_titles}
                   
      t.remove :remarks
    end
  end
end
