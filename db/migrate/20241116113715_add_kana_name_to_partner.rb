
class AddKanaNameToPartner < ActiveRecord::Migration[7.2]
  def change
    change_table :partners do |t|
      t.string :kana_name, null:false #, default:""

      t.string :country, limit:2, null:false
      t.string :hq_addr, null:false
      t.string :trn
    end
    add_index :partners, [:country, :trn], unique:true
    #execute "ALTER TABLE partners ALTER COLUMN kana_name DROP DEFAULT"
  end
end
