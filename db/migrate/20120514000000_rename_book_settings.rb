

class RenameBookSettings < ActiveRecord::Migration
  class << self
    def up
      rename_table "user_settings", "book_settings"
    end
  end
end
