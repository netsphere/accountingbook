
class AddSignToReportingTitle < ActiveRecord::Migration[7.1]
  def change
    change_table :reporting_titles do |t|
      t.column :sign,    "SMALLINT NOT NULL"
      t.column :ac_type, "SMALLINT NOT NULL", comment:"1:残高, 4:発生額"
    end
  end
end
