# -*- coding:utf-8 -*-

# 帳票
class CreateReports < ActiveRecord::Migration
  def self.up
    execute <<EOF
CREATE TABLE reports (
  id       serial PRIMARY KEY,
  book_id  int NOT NULL REFERENCES books (id),
      
  name     VARCHAR(40) NOT NULL,
  description VARCHAR(250) NOT NULL -- 説明
);

CREATE TABLE reporting_titles (
  id               serial PRIMARY KEY,
  -- 親
  report_id        int NOT NULL REFERENCES reports(id),
  
  position         int NOT NULL,   -- for acts_as_list
  account_title_id int NOT NULL REFERENCES account_titles(id),

  -- 科目は複数回, 表示可.
  UNIQUE (report_id, position)
);
EOF
  end
end
