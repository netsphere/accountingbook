

class AccountTitleTreesDropNotNull < ActiveRecord::Migration
  def self.up
    execute <<EOF
ALTER TABLE account_title_trees ALTER COLUMN parent_id DROP NOT NULL;
EOF
  end
end
