
# フィールドが足りてなかった
class DivisionFields < ActiveRecord::Migration[7.1]
  def change
    change_table :divisions do |t|
      t.string :description, null:false
    end

    change_table :division_edges do |t|
      t.boolean "main_parent", null: false
    end

  end
end
