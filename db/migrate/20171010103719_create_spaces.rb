# -*- coding:utf-8 -*-


class CreateSpaces < ActiveRecord::Migration
  def self.up
    execute <<EOF
CREATE TABLE spaces (
  id         serial PRIMARY KEY,
  urlkey     VARCHAR(30) NOT NULL UNIQUE,
  name       VARCHAR(40) NOT NULL,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

CREATE TABLE spaces_users (
  id         serial PRIMARY KEY,
  space_id   int NOT NULL REFERENCES spaces (id),
  user_id    int NOT NULL REFERENCES users (id),
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP,
  UNIQUE (user_id, space_id)
);
EOF
    Book.all.each do |book|
      ac_space = Accounts::Space.find_by_urlkey(book.user_group_code)
      space = ::Space.new
      space.urlkey = ac_space.urlkey
      space.name = ac_space.name
      space.save!

      ac_space.users.each do |ac_user|
        user = ::User.find_by_login(ac_user.login)
        su = SpacesUser.new
        su.space_id = space.id
        su.user_id = user.id
        su.save!
      end
    end

    execute <<EOF
ALTER TABLE books ADD CONSTRAINT books_user_group_code_fkey 
                  FOREIGN KEY (user_group_code) REFERENCES spaces (urlkey)
EOF
  end
end
