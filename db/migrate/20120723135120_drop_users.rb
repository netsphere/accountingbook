# -*- coding:utf-8 -*-

# users, email_logs テーブルを削除
class DropUsers < ActiveRecord::Migration
  def up
    execute "DROP TABLE email_logs"
    #execute "DROP TABLE users"
  end

  def down
    raise ActiveRecord::IrreversibleMigration, "cannot down"
  end
end

