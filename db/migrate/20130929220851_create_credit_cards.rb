# -*- coding:utf-8 -*-

# クレジットカードマスタ
class CreateCreditCards < ActiveRecord::Migration
  def up
    execute <<EOF
CREATE TABLE credit_cards (
  id               serial PRIMARY KEY,
  book_id          int NOT NULL REFERENCES books (id),
  sys_name         VARCHAR(40) UNIQUE,
  name             VARCHAR(40) NOT NULL,
  description      VARCHAR(250) NOT NULL,

  account_title_id int NOT NULL REFERENCES account_titles (id),

  active           boolean NOT NULL,

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
);
EOF
  end

  def down
    drop_table :credit_cards
  end
end
