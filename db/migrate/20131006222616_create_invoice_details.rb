# -*- coding:utf-8 -*-

# 請求書の明細行: 転記方式に移行する
class CreateInvoiceDetails < ActiveRecord::Migration
  def self.up
    execute <<EOF
CREATE TABLE invoice_details (
  id               serial PRIMARY KEY,
  invoice_id       int NOT NULL REFERENCES invoices (id),
  
  lineno           int NOT NULL,
  account_title_id int NOT NULL REFERENCES account_titles (id),
  
  amount           int NOT NULL,

  remarks          VARCHAR(200) NOT NULL,

  created_at       TIMESTAMP NOT NULL
);
EOF

    JournalEntry.where("invoice_id IS NOT NULL").each do |je|
      detail = InvoiceDetail.new :invoice_id => je.invoice_id,
                                 :lineno => 0,
                                 :account_title_id => je.dr_account_id,
                                 :amount => je.amount,
                                 :remarks => je.comment
      detail.save!
    end
  end

  
  def self.down
    drop_table :invoice_details
  end
end
