
# 足りないフィールドを追加
class AddDueDateToJournalEntry < ActiveRecord::Migration[7.1]
  def change
    change_table :journal_entries do |t|
      t.date :due_date
      t.column :vat_excluded, "SMALLINT NOT NULL",
               comment:"period_ends を参照しなくてよいようにする。0:免税 1:税込経理 2:税抜経理"
      t.column :status, "VARCHAR(1) NOT NULL", comment:"未照合='O', 照合済み='C'"
    end
  end
end
