# -*- coding:utf-8 -*-

require_relative "accounts_user"


# books テーブルを作り、各種テーブルはその所有とする
class CreateBooks < ActiveRecord::Migration
  class << self

    def up
      # 既存ユーザID -> 会計帳簿インスタンスID
      @umap = {}
      # メールアドレス -> 集約ユーザID
      # @mail2user = collect_users

      # テーブル名を変更
      execute <<EOF
ALTER TABLE payments RENAME TO cash_slips;
ALTER TABLE payment_amounts RENAME payment_id TO cash_slip_id
EOF

      Accounts::User.transaction do
        create_space_tables
        create_space_instances
        add_space_refs
        # email_set_unique
      end 
    end


    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end


  private
    # 会計帳簿テーブルを作成
    def create_space_tables
      execute <<EOF
CREATE TABLE books (
  id               serial PRIMARY KEY,
  urlkey           VARCHAR(30) NOT NULL UNIQUE,  -- 半角英数; URLの一部を構成
  user_group_code  VARCHAR(30) NOT NULL,
  name             VARCHAR(40) NOT NULL,         -- 日本語での名前
  description      VARCHAR(250) NOT NULL,        -- 説明
  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
)
EOF
    end


    # 既存ユーザのために会計帳簿インスタンスを作成する.
    # user_settings の内容を移す
    def create_space_instances
      tim = Time.utc 2000, 1, 1, 0, 0, 0, 0

      # v3はユーザ = データベース
      ::User.all.each do |user|
        a_user = Accounts::User.find_by_book_user user
        # スペース
        a_ug = Accounts::Space.new( 
                        :urlkey => a_user.login + sprintf("%02.0f", rand * 100),
                        :name => a_user.login + "のスペース",
                        :description => "",
                        :create_user_id => a_user.id )
        a_ug.save!
        a_ugu = Accounts::SpacesUser.new :user_id => a_user.id,
                                         :user_group_id => a_ug.id,
                                         :admin => true
        a_ugu.save!

        book = Book.new :urlkey => a_ug.urlkey,
                        :user_group_code => a_ug.urlkey,
                        :name => user.name + "の家計簿", # 一人で複数ありえる
                        :description => "",
                        :lock_version => 1,
                        :created_at => tim,
                        :create_user_code => a_user.login
        begin
          book.save!
        rescue ActiveRecord::RecordInvalid => e
          p e; p a_ug.urlkey
          raise
        end
        @umap[user.id] = book.id

        # アプリケーションを登録する
        a_app = Accounts::App.new :app_type => "accounting",
                          :app_id => book.id,
                          :user_group_id => a_ug.id
        a_app.save!
      end # each
    end


    def add_basic_columns tbl_name
      raise TypeError if !tbl_name.is_a?(String)
      
      table = Object.const_get tbl_name.classify
      raise TypeError, "expected AR, but #{table.superclass}" if table.superclass != ActiveRecord::Base

      columns = {
        "book_id" => "int REFERENCES books (id)", # NOT NULL

        # 共有されるテーブルは、誰が作成・更新したかを記録
        "lock_version" => "int", # NOT NULL",
        "created_at" => "TIMESTAMP", #  NOT NULL
        "create_user_code" => "VARCHAR(40) REFERENCES users(login)", # NOT NULL
        "updated_at" => "TIMESTAMP",
        "update_user_code" => "VARCHAR(40) REFERENCES users(login)",
      }

      columns.each do |col, typ|
        if tbl_name == "book_settings" && col == "book_id"
          execute "ALTER TABLE #{tbl_name} ADD COLUMN #{col} #{typ} UNIQUE"
        else
          if !table.columns_hash[col]
            execute "ALTER TABLE #{tbl_name} ADD COLUMN #{col} #{typ}"
          end
        end
      end

      # not-null列を設定
      # user_id -> book_id に変更
      tim = Time.utc 2000, 1, 1, 0, 0, 0, 0
      if tbl_name == "book_settings"
        table.all.each do |user_setting|
          book_id = @umap[user_setting.user_id]
          a_user = Accounts::User.find_by_book_user user_setting.user

          # book_settings は id less
          execute <<EOF
UPDATE book_settings
    SET book_id = #{book_id}, lock_version = 1, created_at = '#{tim}',
        create_user_code = '#{a_user.login}'
    WHERE user_id = #{user_setting.user_id}
EOF
        end
      else
        table.all.each do |r|
          book_id = @umap[r.user_id]
          a_user = Accounts::User.find_by_book_user r.user
          # execute()に配列は渡せない
          execute <<EOF
UPDATE #{tbl_name} 
    SET book_id = #{book_id}, lock_version = 1, 
        create_user_code = '#{a_user.login}'
    WHERE id = #{r.id}
EOF
        end # each

        # journal_entries には created_at が元からある => 値がないときだけ更新
        execute <<EOF
UPDATE #{tbl_name} SET created_at = '#{tim}' WHERE created_at IS NULL 
EOF
      end
        
      # 元のuser_idは落とす
      execute "ALTER TABLE #{tbl_name} DROP COLUMN user_id"

      # NOT NULL を付ける
      ["lock_version", "book_id", "created_at", "create_user_code"].each do |c|
        if !(tbl_name == "account_titles" && c == "create_user_code")
          execute "ALTER TABLE #{tbl_name} ALTER COLUMN #{c} SET NOT NULL"
        end
      end
    end
    private :add_basic_columns


    # ユーザの所有から会計帳簿が所有するように変更
    def add_space_refs
      tables = [ "account_titles", "journal_entries", "budgets", "partners", 
                 "invoices", "cash_slips", "book_settings" ]

      execute "ALTER TABLE book_settings DROP CONSTRAINT user_settings_pkey"

      tables.each do |tbl_name|
        add_basic_columns tbl_name
      end
    end

=begin
    # email フィールドでユーザを識別. ユニークに変更
    def email_set_unique
      # ダブりを削除
      (User.find :all).each do |u|
        if @mail2user[u.email] != u.id
          execute "DELETE FROM email_logs WHERE user_id = #{u.id}"
          execute "DELETE FROM users WHERE id = #{u.id}"
        end
      end

      execute "ALTER TABLE users ADD CONSTRAINT users_email_key UNIQUE (email)"
    end
=end

  end # class << self
end
