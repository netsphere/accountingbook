# -*- coding:utf-8 -*-

# 勘定科目は薔薇木にする
class CreateAccountTitleTrees < ActiveRecord::Migration

  ACTYPE_SIGNS = [nil, +1, -1, -1, -1, -1, -1, -1, -1]


  def self.up
    # 親子関係. 物理ツリー
    execute <<EOF
CREATE TABLE account_title_trees (
  id        serial PRIMARY KEY,
  parent_id int NOT NULL REFERENCES account_titles (id),
  child_id  int NOT NULL REFERENCES account_titles (id),
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login),
  UNIQUE (parent_id, child_id)
);
EOF

    add_columns = [
      ["aggregate", "boolean"],
      # ["system_code", "int"],
      ["sign", "int"],
    ]  
    add_columns.each do |x|
      execute "ALTER TABLE account_titles ADD COLUMN #{x[0]} #{x[1]}"
    end

    Book.all.each do |book|
      ac_map = {}
      
      # システム科目を登録する
      DEFAULT_AC_ITEMS.each do |data|
        next if !data[1]  # non-system

        acr = book.new_ac_from_template_line data
        acr.ac_type = -1
        acr.cf_type = -1  # 後で消す
        acr.save!

        ac_map[data[0]] = acr if data[2]        

        # 親の子にする
        if data[6] > 0 
          acr.connect_parent! ac_map[data[6]]
        end
        if data[7] > 0
          acr.connect_parent! ac_map[data[7]]
        end
      end # each

      # 既存の実在科目は, 集計科目の子にする
      AccountTitle.where("book_id = ? AND (aggregate IS NOT TRUE)", book.id) \
                  .each do |ac|
        if ac.cf_type == 4 # 資金(現金・現金同等物)
          ac.connect_parent! ac_map[14] # magic!
        else 
          ac.connect_parent! ac_map[ac.ac_type]
          if ac.ac_type >= 1 && ac.ac_type <= 3
            # 収益・費用は C/Fには表示させない。C/F計算書は純利益から出発する
            ac.connect_parent! ac_map[ac.cf_type + 10]
          end
        end

        # 貸借など追加フィールド
        ac.aggregate = false
        #ac.system_code = nil
        ac.sign = ACTYPE_SIGNS[ac.ac_type]
        ac.name = ac.name.blank? ? "_" : ac.name
        ac.save!
      end
    end # Book.all.each

    # スキーマに制約を付ける
    add_columns.each do |x|
      if x[0] != "system_code"
        execute "ALTER TABLE account_titles ALTER COLUMN #{x[0]} SET NOT NULL"
      end
    end
=begin    
    execute <<EOF
ALTER TABLE account_titles ADD CONSTRAINT account_titles_book_id_system_code_key
                               UNIQUE(book_id, system_code)
EOF
=end
  end


  def self.down
    raise "cannot down"
  end
end
