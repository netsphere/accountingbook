
# 勘定科目を整える
class AccountTitles < ActiveRecord::Migration[7.1]
  def change
    # 勘定科目の hierarchies テーブル:
    # <s>閉包テーブル closure table でいく。</s> NG.
    # 作表例 https://docs.oracle.com/en/middleware/bi/analytics-server/metadata-oas/create-and-manage-dimensions-parent-child-hierarchies.html#GUID-AC9FB1D9-59FE-4C72-811D-27148FAB3A6B
    # ただし,
    #  - 自分自身を指すレコード distance = 0 は登録しない。
    #  - root は, 自分が child 側に出現しない、で識別する.
    change_table :account_title_edges do |t|
      # 主たる親フィールド. 複数の親を持てるようにする
      t.boolean :main_parent, null:false
      
      # 自分自身 (distance = 0) は登録しない. 先祖を全部登録する
      #t.integer :distance, null:false

      # -1 = 足し上げのときに符号反転。
      t.integer :weight, null:false
    end
    
    # 途中 node にも金額を登録してよい。(科目を分割できるため)
    #add_column :account_titles_tree, :isleaf, :boolean, null:false

    # 勘定科目テーブルはグローバルに持つ。
    # 各会計帳簿での有効/無効を設定する
    create_table :ac_ovrds do |t|
      # 32bit int にする. 型を合わせる
      t.references :account_title, type: :integer, null:false, foreign_key:true
      
      t.column :book_id, 'VARCHAR(14) NOT NULL REFERENCES books(id)'
      
      t.boolean :active, null:false
      t.string  :name,   null:false
      t.string  :name_short, null:false
      t.column  :sign,  "SMALLINT NOT NULL", comment:"元帳の表示正残. 借方 = +1"
      
      t.timestamps
    end
    add_index :ac_ovrds, [:account_title_id, :book_id], unique:true

=begin
    change_table :account_titles do |t|
      #t.remove :book_id
      #t.remove :system_code

      # システム科目は削除不可
      #t.boolean :system_item, null:false
    end
=end
  end
end
