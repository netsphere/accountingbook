# -*- coding:utf-8 -*-

# クレジットカードトランザクション
class CreateCcTransactions < ActiveRecord::Migration
  def create_cc_transactions
    execute <<EOF
CREATE TABLE cc_transactions (
  id               serial PRIMARY KEY,
  book_id          int NOT NULL REFERENCES books (id),

  credit_card_id   int NOT NULL REFERENCES credit_cards (id),

  due_date         DATE NOT NULL,

  -- 取引日 (利用日)
  date             DATE NOT NULL,

  -- 利用店名
  shop_name        VARCHAR(250) NOT NULL,

  -- 1 のとき家族カード
  family_flag      int NOT NULL,

  -- ex) "1回"
  payment_type     int NOT NULL,

  -- 合計 ...引き落とし時値引きがあることがある => 明細行を作る
  -- 明細行は payment_amounts
  amount           int NOT NULL,

  -- 備考
  remarks          VARCHAR(250) NOT NULL,

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login)
);
EOF
  end


  def create_cc_payments
    execute <<EOF
CREATE TABLE cc_payments (
  id           serial PRIMARY KEY,

  cash_slip_id      int NOT NULL REFERENCES cash_slips (id),

  cc_transaction_id int NOT NULL REFERENCES cc_transactions (id),

  amount            int NOT NULL,

  UNIQUE (cash_slip_id, cc_transaction_id)
);
EOF
  end


  def up
    create_cc_transactions()

    execute <<EOF
ALTER TABLE payment_amounts ALTER COLUMN cash_slip_id DROP NOT NULL;
ALTER TABLE payment_amounts ADD COLUMN cc_transaction_id int REFERENCES cc_transactions (id);
ALTER TABLE payment_amounts ADD CONSTRAINT payment_amounts_cc_transaction_id_invoice_id_key UNIQUE (cc_transaction_id, invoice_id)
EOF

    create_cc_payments()
  end


  def down
    raise "cannot down"
    drop_table :cc_transactions
  end
end
