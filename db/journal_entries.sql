-- -*- coding:utf-8 -*-

-- 仕訳（1行の）
--
-- 1枚の振替伝票は, (date, entry_no) で区別する.
-- TODO: 振替伝票の表示行を追加する
CREATE TABLE journal_entries (
  id         serial PRIMARY KEY,
  book_id    VARCHAR(14) NOT NULL REFERENCES books (id),

  date       DATE NOT NULL,  -- 日付
  entry_no   int NOT NULL,   -- 伝票番号

  -- 借方, 貸方
  dr_account_id int NOT NULL REFERENCES account_titles (id),
  dr_partner_id int REFERENCES partners (id),
  cr_account_id int NOT NULL REFERENCES account_titles (id),
  cr_partner_id int REFERENCES partners (id),

  -- 金額. 貸方は符号反転
  -- `NUMERIC` は厳密に指定精度, `DECIMAL` は指定以上の精度、通常は後者を使え
  amount     DECIMAL(12, 2) NOT NULL,

  -- 後続の migration で削除する
  remarks    VARCHAR(100) NOT NULL, -- 相手 for compat.
  
  comment    VARCHAR(250) NOT NULL, -- 内容

  -- 転記元
  invoice_id int REFERENCES invoices(id),
  payment_amount_id int REFERENCES payment_amounts(id),  -- 支払い

  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,  -- 作成時刻
  create_user_code VARCHAR(40) NOT NULL REFERENCES users(login),
  updated_at       TIMESTAMP, -- 更新時刻
  update_user_code VARCHAR(40) REFERENCES users(login)
);
