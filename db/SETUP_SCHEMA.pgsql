\i schema_migrations.sql
\i users.sql
/* \i email_logs.sql */
\i spaces.sql
\i spaces_users.sql
\i books.sql      /* v4 追加 */
\i account_titles.sql
\i account_title_edges.sql
\i book_settings.sql
\i partners.sql
\i invoices.sql
\i invoice_details.sql
/*\i cash_slips.sql */

\i budgets.sql
\i budget_details.sql

\i cash_accounts.sql
\i expenses.sql
\i cc_reconciliations.sql
\i cc_reconc_details.sql

\i payments.sql
\i payment_amounts.sql
\i journal_entries.sql
\i period_ends.sql
\i reports.sql
\i reporting_titles.sql
