# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# 勘定科目
# テスト用に, 集計科目, 実在科目それぞれで,
#   AcOvrd なし, active = Yes, active = No
#   AcOvrd あり, sys active = Yes, book active = Yes
#                sys active = Yes, book active = No
#                sys active = No,  book active = No

  # JIS コードはおかしすぎる。何で P/L が 8xxx と 9xxx だけなんだ.
# 支払利息
   
  #["A2320", name:"建物_売却収入", name_short:"建物の売却収入"},  ●ここじゃない


# 会計部門 ###########################################################

# テスト用に, 集計部門, 実在部門,
#   AcOvrd なし
#   AcOvrd あり, active = Yes
#                active = No

require 'csv'

def import_divisions
  CSV.read("db/default_divisions.csv").each_with_index do |line, idx|
    next if idx < 2
    next if line[0].blank? || line[0][0, 1] == "#"

    aggregate = line[1].blank? ? false : (line[1] == "●" ? true : raise)
    vat_disable = line[4].blank? ? false : (line[4].to_i == 1 ? true : raise)
    div = Division.create! book_id: nil,
                   division_code: line[0],
                   name: line[2],
                   aggregate: aggregate,
                   description: (line[5] || ""),
                   # 実在部門のみ
                   purchase_active: !aggregate,
                   vat_disable: (aggregate ? false : vat_disable)

    # 親子関係
    if line[3] != "☆"
      parent = Division.where('book_id IS NULL and division_code = ?', line[3]).take
      parent.add_child div
    end
  end
end


def import_vats
  CSV.read("db/default_vats.csv").each_with_index do |line, idx|
    next if idx < 2
    next if line[0].blank? || line[0][0, 1] == "#"
  
    country = line[0][0, 2]
    raise if country != line[1]
  
    Vat.create! active: true,
              country: line[1],
              tax_group: line[2],
              tax_code: line[0],
              name: line[3],
              effective_day: line[4],
              tax_rate: line[5],
              description: (line[6] || "")
  end
end


def import_account_titles
  ac_list = {}

  CSV.read("db/default_account_titles.csv").each_with_index do |line, idx|
    next if idx < 2
    next if line[0].blank? || line[0][0, 1] == "#"

    aggregate = line[1].blank? ? false : (line[1] == "●" ? true : raise)
    ac_type = line[5] == "P" ? 4 : (line[5] == "B" ? 1 : raise(line.inspect) )
    kind = case (line[6] || '').upcase
             when "CASH"; 4  # 資金
             when "AR";   1  # 債権
             when "AP";   2  # 債務
             else         0
           end
    ac = AccountTitle.create! book_id: nil,
                       aggregate: aggregate,
                       ac_type: ac_type,
                       sort: line[0],  
                       name: line[2],
                       name_short: (line[3].blank? ? nil : line[3]),
                       end_day: nil,
                       description: (line[10] || ""),
                       # 実在科目のみ
                       sign: line[7],  # これは"元帳"の表示正残
                       kind: kind,
                       enable_invoice: false,
                       payable_id: nil,
                       tax_group: (aggregate ? 0 : (line[8] || 0))
    raise(line.inspect)  if ac_list.has_key?(ac.sort)
    ac_list[ac.sort] = ac
    
    if line[4] != "☆"
      parent = AccountTitle.where('book_id IS NULL and sort = ?', line[4]).take
      raise line.inspect if !parent || !parent.aggregate
      parent.add_child ac, {weight: 1}
    end
  end

  # 購買科目の相手勘定. もう一周、回す
  CSV.read("db/default_account_titles.csv").each_with_index do |line, idx|
    next if idx < 2
    next if line[0].blank? || line[0][0, 1] == "#"

    if !line[9].blank?
      ac = AccountTitle.where('book_id IS NULL and sort = ?', line[0]).take
      next if ac.aggregate
      
      ap = AccountTitle.where('book_id IS NULL and sort = ?', line[9]).take
      raise(line.inspect) if !ap || ap.kind != 2
      ac.enable_invoice = true
      ac.payable_id = ap.id
      ac.save!
    end    
  end
  
end

ActiveRecord::Base.transaction do
  # 先にユーザをつくってから実行すること
  u = User.find_by_email "hisashi.horikawa@gmail.com"
  u.admin_role = true
  u.save!
  
  import_divisions()
  import_vats()
  import_account_titles()
end

