
-- ユーザ  -*- coding:utf-8 -*-
-- `20240312131358_sorcery_core.rb` で変更を入れる
CREATE TABLE users (
  id                serial PRIMARY KEY,
  login             VARCHAR(40) NOT NULL UNIQUE,   -- ログインID

  -- メールアドレス
  email             VARCHAR(40) NOT NULL,                          -- 後で UNIQUE
  crypted_password  VARCHAR(40) NOT NULL, -- 暗号化したパスワード  -- 後で削除

  -- 認証のバージョンアップ. SHA256 = 64文字
  crypted_password2 VARCHAR(64),                                   -- 後で名称変更
  salt              VARCHAR(64),

  name              VARCHAR(40) NOT NULL, -- 氏名

  -- 有効期限
  expire            TIMESTAMP,

  created_at        TIMESTAMP NOT NULL,
  updated_at        TIMESTAMP
);
