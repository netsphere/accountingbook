-- -*- coding:utf-8 -*-

-- 勘定科目
-- 親子関係は `account_title_hierarchies` テーブルで定義する.
--
-- 総勘定元帳で P/L科目を適切に表示するため, 次のいずれかの方法がある;
--   ▲ 科目マスタに B/S or P/Lフラグを持つ, さらに決算日マスタを設ける.
--     => △ 加算先と矛盾があった場合にどうするか???
--   ☆ こっちでいく- 決算日に, 調整伝票で、未処分利益に振り替える.
--     => ○ 自由度高い (ほかの科目への振替も可能)
--        △ 締め後投入されると面倒.
--        この方法でも, 未来の表示のため, 決算日マスタは必要.
--     => ○ B/S は全期間を集計するので, P/L だけ節約する意味が乏しい.
CREATE TABLE account_titles (
  id          serial PRIMARY KEY,

  -- ナル値の場合, システム科目 (ユーザは変更不可)
  book_id     VARCHAR(14) REFERENCES books (id),

  -- 集計科目のとき true. あとから変更不可.
  aggregate   boolean NOT NULL,

  -- 実在科目のみ: 種類. あとから変更不可.
  --   1 = B/S科目 Balance
  --   4 = P/L科目 Period
  ac_type     SMALLINT NOT NULL,

  -- 1 = (販売伝票の)債権科目, 2 = (購買伝票の)債務科目, 4 = 資金,
  -- 0 = その他
  kind        SMALLINT NOT NULL,

  -- 勘定科目コード.
  sort        VARCHAR(8) NOT NULL,          
  
  -- ナル値以外の時 => タクソノミを参照。ユーザによる加算先の削除不可
  -- トップレベル(資産、負債、純資産、純利益、ほか特殊科目)
  -- UNIQUEを付ける.
  -- system_code int,

  name        VARCHAR(40) NOT NULL,  -- 名前
  -- 携帯電話用
  name_short  VARCHAR(20) ,  

  description VARCHAR(250) NOT NULL, -- 説明

  -- 総勘定元帳: 表示上の正残. 借方科目 = +1, 貸方科目 = -1
  sign        SMALLINT NOT NULL,

  -- active      boolean NOT NULL,          -- false のとき仕訳の新規登録不可
  end_day        DATE,
  
  -- 購買請求書・経費精算で費用性科目として登録可.
  enable_invoice   boolean NOT NULL,

  -- 買掛金・未払金a/c
  payable_id       int REFERENCES account_titles(id),
  
  lock_version     int NOT NULL,
  created_at       TIMESTAMP NOT NULL,

  -- データ移行時はナル値.
  create_user_code VARCHAR(40) REFERENCES users(login),
  updated_at       TIMESTAMP,
  update_user_code VARCHAR(40) REFERENCES users(login),

  -- UNIQUE (book_id, system_code)
  UNIQUE (book_id, sort)
);
