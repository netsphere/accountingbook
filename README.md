
● TODO:
  勘定科目, 保存時エラー `active` attribute がない。 -> form 上, 日付にすること



# Beyond Accounting 超会計 -- former Q's Web Accounting Book

A small personal finance software.


## What's this?

 - フリーランサー・個人事業主向け. 家計簿としても利用可能。
 - 簡素で役に立つ機能を取捨選択.
 - しっかりした会計システムを基礎として、その上に請求書処理などのインボイス機能、資金計画機能を載せる

コンセプト:
 - 複式簿記で資産・負債も記録。
 - 事業所得と生活費の両方を一体的に扱えるようにする。"under one roof"
 - 消費税、インボイスをサポート
 - 資金計画. 積立計画, ZBB

機能と、進捗状況はこちら; https://www.nslabs.jp/ac/


## Licence

Licence: GNU AGPLv3+

Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.



## How to run

```shell
# su postgres
$ createdb --encoding UTF-8 --owner rails  accounting4_dev
```

スキーマを読み込む

```shell
$ psql -h localhost -U rails accounting4_dev
accounting4_dev=> \i SETUP_SCHEMA.pgsql 
```

さらに migration, seed data 読み込み。

```shell
$ bin/rails db:migrate
$ bin/rails db:seed
$ bin/rails db:migrate:status
```

実行!

```shell
$ passenger start
```



## Get Involved

I encourage you to contribute to <i>Beyond Accounting 超会計</i>!
Please check out <a href="https://www.nslabs.jp/contributor-license-agreement.rhtml">Individual Contributor License Agreement</a>





## 余談

### 借方, 貸方を Dr, Cr と書くのはなぜか?

Latin <i>debitor</i> (イタリア語 debitore)
  -> Eng. DEBTOR (= one who owes a debt.)
English CREDITOR (= one to whom a debt is owed.)

現代では, 借方は "debit column" と呼ばれる。列のヘッダに Debtor と書かれることもある。
単に Debit, Credit と書かれることも多い:
    ex. https://www.accountingformanagement.org/accounts-payable/

歴史 "https://egrove.olemiss.edu/aah_journal/vol13/iss2/12/">Where's the R in debit?</a>


