
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# ルーティング
Rails.application.routes.draw do

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Render dynamic PWA files from app/views/pwa/*
  get "service-worker" => "rails/pwa#service_worker", as: :pwa_service_worker
  get "manifest" => "rails/pwa#manifest", as: :pwa_manifest


  # You can have the root of your site routed with "root"
  root :to => 'welcome#index'

  # 単数形は `controller:` でクラスを指定.
  resource :account, controller:'account' do
    # Sorcery と名前が被る。`login`, `logout` は避ける
    get 'sign_in'
    post 'sign_out'
  end

  # RP として振る舞う
  namespace :connect do
    # とりあえず Google を使う. TODO: `accounts.nslabs.jp` に切り替えること
    resource :google_oauth2, only:[:create, :show], controller:'google_oauth2'
  end


  # グローバルマスタ########################################################
  
  # 消費税マスタ
  resources :vats

  # 為替レート
  # ●●
  
  
  resources :books do
    # 管理 ########################################################
    
    member do
      # 締め処理
      get :period_end_close
    end

    # 会計期間
    resources :period_ends

    
    # 財務会計 ########################################################
    
    # 会計部門マスタ
    resources :divisions do
      member do
        post :enable_div
      end
    end
    
    # 勘定科目マスタ
    resources :account_titles do
      member do
        post :enable_ac
      end
    end

    # 仕訳帳・振替伝票
    resources :journals do
      # `:id` 伝票番号 + date= 伝票日付
      member do 
        post 'add_line'
      end
      collection do # `:id` なし
        get 'list'
        #post 'remove_line' クライアント側で実装
      end
    end
    
    # 予定取引
    # ●●

    # 総勘定元帳.
    # `:id` は不要. コントローラは `GeneralLedgersController`
    resource :general_ledger do
      collection do
        get 'list'
      end
    end

    # 簡易な固定資産
    # ●●

    # 予算 -- バージョン管理があるから複数形
    resources :budgets do
    end

    # 科目別税区分別消費税集計表
    # ●●

    # 財務諸表テンプレート
    resources :report_templates do
    end

    # 財務諸表テンプレートの各科目
    resources :reporting_titles do
      collection do
        #post 'new_line'
      end

      member do
        post 'edit_cxl'
      end
    end

    # 財務諸表・予算実績対比
    resource :statements do
      collection do
        get 'list'
        # 月次推移
        get 'month'
      end
    end

    
    # 販売管理 ########################################################

    # 販売請求書
    # ●●

    
    # 購買管理 ########################################################

    # (購買) 請求書
    resources :invoices

    # 経費精算
    resources :expenses

    
    # 取引先 ########################################################

    # 取引先マスタ
    resources :partners do
      collection do
        get "search"
      end
    end

    
    # 入出金 ########################################################

    # 取引照合: Credit Card Reconciliation
    resources :cc_reconciliations

    # 銀行振込み
    resources :payments

    # 出納帳
    # `:id` は不要
    resource :cashbook

    # 資金口座マスタ
    resources :cash_accounts
  
    # 金融投資
    # ●●

    # 借入金
    # ●●

  end
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
