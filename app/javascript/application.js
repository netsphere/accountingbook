// Entry point for the build script in your package.json

// Turbo
import "@hotwired/turbo-rails"

// Stimulus
import "./controllers"
