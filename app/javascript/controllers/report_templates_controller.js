
import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="report-templates"
export default class extends Controller
{
    static targets = ["insPos"];
    
    // @override
    connect() {
        console.log("connected");
    }

    // callback
    onPosSelect(event) {
        console.log(event);
        this.insPosTarget.value = event.target.value;
    }
    
}
