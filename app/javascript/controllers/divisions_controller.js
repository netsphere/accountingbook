
import { Controller } from "@hotwired/stimulus";
//import {TabulatorFull as Tabulator} from 'tabulator-tables';


// Connects to data-controller="divisions"
export default class extends Controller
{
    static targets = ["tbl"];

    // @param ev [PointerEvent]
    // @param cell  _cell > { row , table }
    //              row > data -- これがモデルデータ
    async onActiveClicked(ev, cell) {
        const data = cell._cell.row.data;
        const response = await fetch(`./${data.id}/enable_div`, {
                                method: "POST",
                                headers: {
                                    "X-CSRF-Token": document.querySelector("[name='csrf-token']").content,
                                    //"Turbo-Frame": "div-frame",
                                    "Accept": "text/vnd.turbo-stream.html", // turbo stream 
                                }});
        // リダイレクトではなく, 表だけ再描画
        const message = await response.text();
        //const frame = document.getElementById('div-frame').innerHTML = message;
        Turbo.renderStreamMessage(message);
    }
    
    // @override
    connect() {
        const tableData = JSON.parse(document.getElementById('tbl-json-data').textContent);
        
        //console.log(tableData);
        this.tbl = new Tabulator("#div-tbl", {
            pagination:true, //enable.
            paginationSize: 25, // this option can take any positive integer value
            data: tableData, //set initial table data
            dataTree: true,
            dataTreeSort:false, //disable child row sorting
            dataTreeStartExpanded:true, // 全部開く
            //autoColumns:true,
            columns: [
                {title:"名前", field:"name", width:200, sorter:"string"},
                {title:"集計?", field:"aggregate", sorter:"boolean"},
                {title:"部門コード", field:"division_code", sorter:"string"},
                {title:"Active", field:"active", formatter:"tickCross", sorter:"boolean", cellClick: this.onActiveClicked },
                {title:"消費税不可?", field:"vat_disable", sorter:"boolean"},
                {title:"購買で有効?", field:"purchase_active", sorter:"boolean"},
                {title:"", field:"show", formatter:"link"},
            ]
        });
    }
}
