
// `select` タグを差し替える.
// 勘定科目の選択 => デフォルト税コードの切り替え

import { Controller } from "@hotwired/stimulus"
import Choices from "choices.js";

export default class extends Controller
{
    // `data-コントローラ名-target` でターゲットを指定.
    //   <div data-controller="s​earch">
    //     <div data-search-target="results"></div>
    //   </div>
    // 自動的に `this.xxxTarget` というプロパティが生える. 名前にハイフン不可
    // `this.xxxTargets` も生える. 同名の要素の配列.
    static targets = [
        "acSelect", "divSelect", "partnerSelect", "vatSelect",
        "debitAmt", "creditAmt", "drVatAmt", "crVatAmt"];

    // @override
    connect() {
        this.acOptions = JSON.parse(document.getElementById("ac-json-data").textContent);
        const ac = this.lookup_option(this.acOptions,
                        Number(this.acSelectTarget.getAttribute('value')));
        console.log('ac = ', ac);
        this.acChoices = new Choices(this.acSelectTarget,
                                     {shouldSort:false, 
                                      itemSelectText: '',
                                      classNames: {
                                          itemSelectable: 'my-selectable',
                                      },
                                     } ); 
        this.acChoices.setChoices(this.acOptions);//.map((x) => {
        if (ac !== null)
            this.acChoices.setChoiceByValue(ac.value);
        
        this.vatOptions = JSON.parse(document.getElementById('vat-json-data').textContent);
        this.vatChoices = new Choices(this.vatSelectTarget,
                                      {shouldSort: false,
                                       itemSelectText: '',
                                       classNames: {
                                           itemSelectable: 'my-selectable',
                                       },
                                      } );
        // 選択肢を絞る
        const vat_value = Number(this.vatSelectTarget.getAttribute('value'));
        this.set_vat_choices(ac, vat_value);
    }

    // callback
    amtUpdate(event) {
        console.log("vatSelect.value = ", this.vatSelectTarget.value);
        const vat = this.lookup_option(this.vatOptions,
                                       Number(this.vatSelectTarget.value));
        console.log(vat);
        
        const dr_amt = this.debitAmtTarget.value;
        const cr_amt = this.creditAmtTarget.value;
        if (vat !== null) {
            const rate = vat.tax_rate / 100.0;
            this.drVatAmtTarget.value = (dr_amt / (1.0 + rate)) * rate;
            this.crVatAmtTarget.value = (cr_amt / (1.0 + rate)) * rate;
        }
    }

    
    // マスタレコードを探す
    // @return 見つからなかったとき null
    lookup_option(ary, value) {
        console.log(ary, value); // debug
        let found = null;
    LABEL:
        for (let i = 0; i < ary.length; ++i) {
            const opts = ary[i].choices;
            if (opts !== undefined) {
                // option_groups の場合
                for (let j = 0; j < opts.length; ++j) {
                    if (opts[j].value === value) {
                        found = opts[j];
                        break LABEL;
                    }
                }
            }
            else {
                if (ary[i].value === value) {
                    found = ary[i];
                    break;
                }
            }
        }

        return found;
    }


    // VAT: 選択肢を絞る
    // ac の tax_group を確認. S か P か消費税不可か.
    // effective_day の外側のも削除
    // @param ac          null あり.
    // @param cur_vat_id  null あり.
    set_vat_choices(ac, cur_vat_id) {
        if (ac === null)
            return; // 絞らない
        
        const posting_date = document.getElementById('journal_date').value;

        const vatSide = ac.tax_group > 0 && ac.tax_group < 200 ? "S" :
                        (ac.tax_group >= 200 && ac.tax_group < 400 ? "P" : '');
        
        let vats = [{label:"--", value:0}];
        if (vatSide === '') {
            vats = []; // 消費税不可. 選ばせない
            //this.vatChoices.clearInput();
            //this.vatChoices.clearChoices();
            this.vatChoices.clearStore();
        }
        else {
            // `filter()` は shallow copy になる。
            vats = vats.concat(this.vatOptions.filter((vat) => {
                return (vatSide === 'S' ? vat.tax_group < 200 : vat.tax_group >= 200) &&
                       vat.effective_day <= posting_date;
            }) );

            // tax_group が同じで最初の (effective dayが最近) を選択
            let selected = -1;
            for (let i = 0; i < vats.length; ++i) {
                if (cur_vat_id === vats[i].value) {
                    vats[i].selected = true;
                    if (selected >= 0)
                        vats[selected].selected = false;
                    break;
                }
                if (vats[i].tax_group === ac.tax_group) {
                    //vats[i] = JSON.parse(JSON.stringify(vats[i]));
                    if (selected < 0 ) {
                        vats[i].selected = true; selected = i;
                    }
                    //break;  ここは break しない
                }
            }
            this.vatChoices.setChoices(vats, 'value', 'label',
                                       true); // replaceChoices
        }
    }

    
    // callback
    // @param event [CustomEvent]
    //              params: {...},
    //              detail: {value:355},
    //              srcElement: select#entries_0_account_id
    //              target: select#entries_0_account_id
    //              timeStamp: 35223....,
    //              type: "change"
    onAcChanged(event) {
        const ac = this.lookup_option(this.acOptions, event.detail.value);
        this.set_vat_choices(ac, null);
    }

    onRemoveLine(event) {
        if ( window.confirm("OK?") ) {
            // TODO: ●impl.
        }
    }
}
