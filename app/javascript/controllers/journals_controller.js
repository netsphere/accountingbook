
// 縦に金額を合計する。

import { Controller } from "@hotwired/stimulus";

// `data-controller` でコントローラ class を指定し,
export default class extends Controller
{
    // 名前にハイフン不可.
    static targets = ["debitAmt", "creditAmt", "vatAmt", "vatSide"];

    // @override
    connect() {
    }

    // 合計行を更新
    update_ttl_field(ary, ttl_elm) {
        let ttl = 0;
        for (let i = 0; i < ary.length; ++i) 
            ttl += Number(ary[i].value);

        const div = document.getElementById(ttl_elm);
        div.innerHTML = ttl;
    }
    
    // callback
    debitUpdate(event) {
        this.update_ttl_field(this.debitAmtTargets, 'debit_ttl');
    }

    // callback
    creditUpdate(event) {
        this.update_ttl_field(this.creditAmtTargets, 'credit_ttl');
    }

    // callback
    vatUpdate(event) {
        input_vat = 0; output_vat = 0;
        for (let i = 0; i < ary.length; ++i) {
            amt = this.vatAmtTargets[i].value;
            if (amt !== 0) {
                if (this.vatSideTargets[i].value === "P")
                    input_vat += amt;
                else if (this.vatSideTargets[i].value === "S")
                    output_vat += amt;
            }
        }

        document.getElementById('input_vat').innerHTML = input_vat;
        document.getElementById('output_vat').innerHTML = output_vat;
    }

    
    // [+] button click
    async onAddLine(event) {
        const query = new URLSearchParams({
                        lineno: event.params.lineno || 0,
                        date: document.getElementById("journal_date").value});
        const response = await fetch(`add_line?${query}`, {
                                method: "POST",
                                headers: {
                                    "X-CSRF-Token": document.querySelector("[name='csrf-token']").content,
                                    "Accept": "text/vnd.turbo-stream.html",
                                } });
        const message = await response.text();
        Turbo.renderStreamMessage(message);
    }

}
