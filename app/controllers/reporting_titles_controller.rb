
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 報告様式マスタの各科目
class ReportingTitlesController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book
  
  before_action :set_title, only: %i[ edit edit_cxl update destroy ]

  
  # POST
  # 1行追加
  def create
    @report = Report.find params[:report_id]
    raise "internal error" if @report.book_id != @book.id

    ReportingTitle.transaction do
      #`:account_title_id` に配列が入ってくる. なぜか必ずブランクがある.
      pos = nil
      params[:reporting_title][:account_title_id].each do |ac_id|
        next if ac_id.to_i == 0

        if !pos
          pos = params[:reporting_title][:position].to_i
          pos = -1 if pos == 0 
        end
        reporting_title = ReportingTitle.new(
                            report_id: @report.id,
                            account_title_id: ac_id.to_i,
                            sign: title_params[:sign],
                            ac_type: title_params[:ac_type],
                            position: nil )
        @report.insert_title pos, reporting_title
        pos += 1 if pos > 0
      end
    end
    redirect_to(controller:"report_templates", action:"show", id:@report.id)
  end

  
  def edit
    @ac_list = AccountTitle.grouped_items(@book, false)
    # これは Turbo Frames で返す
    render "edit", layout:false
  end
  
  # POST
  def edit_cxl
    #id = params[:id].to_i
    @report = @reporting_title.report
    render partial:"reporting_title", locals:{title:@reporting_title}
  end

  
  def update
    @reporting_title.assign_attributes(title_params)
    if @reporting_title.save
      render partial:"reporting_title", locals:{title:@reporting_title}
    else
      render "edit", status: :unprocessable_entity, layout:false
    end
  end

  # DELETE
  def destroy
    @reporting_title.destroy!
    render turbo_stream: turbo_stream.remove(@reporting_title)
  end

  
private
  def set_title
    @reporting_title = ReportingTitle.find params[:id]
    report = @reporting_title.report
    raise ActiveRecord::RecordNotFound if report.book_id && report.book_id != @book.id
  end

  def title_params
    params.require(:reporting_title).permit(:account_title_id, :sign, :ac_type)
  end
end
