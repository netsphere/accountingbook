
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 会計帳簿。アプリケィションの中心。
class BooksController < ApplicationController
  #before_action :set_book, only: %i[ show edit update destroy ]

  # @book の設定を兼ねる
  before_action :check_if_user_can_access_book, except: %i[index new create]


  # 自分がアクセス可能な家計簿の一覧
  # GET /books or /books.json
  def index
    @books = current_user.accessable_books
  end


  # GET /books/1 or /books/1.json
  def show
  end

  # GET /books/new
  def new
    @book = Book.new
    @book_setting = BookSetting.new
  end

  # 会計帳簿の設定
  # GET /books/1/edit
  def edit
    @book_setting = @book.setting
  end

  # POST /books or /books.json
  def create
    @book = Book.new(book_params)
    @book.create_user_code = current_user.login
    @book.update_user_code = current_user.login
    @book_setting = BookSetting.new(params.require(:book_setting).permit(:unappropriated_item))

    begin
      ActiveRecord::Base.transaction do
        @book.save!
        books_user = BooksUser.create!(book_id: @book.id,
                                       user_id: current_user.id,
                                       member_type: BooksUser::OWNER)
        @book_setting.book_id = @book.id
        @book_setting.closing_day = 31  # TODO: このフィールド削除
        @book_setting.entry_no_counter = 1
        @book_setting.create_user_code = current_user.login
        @book_setting.update_user_code = current_user.login
        @book_setting.save!
      end
      redirect_to( {action:"show", id: @book.id},
                   notice: "Book was successfully created." )
    rescue ActiveRecord::RecordInvalid => e
      render :new, status: :unprocessable_entity 
    end
  end


  # PATCH/PUT /books/1 or /books/1.json
  def update
    @book.attributes = book_params
    @book_setting = @book.setting
    @book_setting.attributes = params.require(:book_setting).permit(:unappropriated_item)
    
    begin
      ActiveRecord::Base.transaction do
        @book.save!
        @book_setting.save!
      end
      redirect_to( @book, notice: "Book was successfully updated." )
    rescue ActiveRecord::RecordInvalid
      render :edit, status: :unprocessable_entity 
    end
  end


  # DELETE /books/1 or /books/1.json
  def destroy
    @book.destroy
    redirect_to books_url, notice: "Book was successfully destroyed." 
  end


  # 締め処理 (表示)   -> ロックだけ?
  def period_end_close
  end

  
private

  # Only allow a list of trusted parameters through.
  def book_params
    # `fetch()` はデフォルト値を定める
    #params.fetch(:book, {})
    params.require(:book).permit(:name, :description)
  end

end # class BooksController
