
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "ostruct"

class GLPeriodTerm
  include ActiveModel::Validations
  include ApplicationHelper
  
  # ●● TODO: 範囲を指定できるようにすべき。
  attr_reader :ac_id

  attr_reader :div_id

  # 自
  attr_reader :from
  # 至
  attr_reader :to

  validates_presence_of :from
  validates_presence_of :to
  
  validates_presence_of :ac_id
  validate :check


  def initialize params = {}
    @from = date_from_str params[:from]
    @to   = date_from_str params[:to]
    @ac_id = params[:ac_id]
    @div_id = params[:div_id]
  end

  
private
  # for `validate()`
  def check
    errors.add(:to, "must be from <= to") if from > to
  end
end


# 総勘定元帳 (単数形リソース)
class GeneralLedgersController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book


  # 検索条件
  # GET /general_ledgers/
  def show
    today = Date.today
    @term = GLPeriodTerm.new(
                    from:Date.new(today.year, today.mon, 1),
                    to:  Date.new(today.year, today.mon, 1).next_month - 1)
  end


  # 総勘定元帳の表示
  def list
    @term = GLPeriodTerm.new params[:gl_period_term]
    if !@term.valid?
      render 'show', status: :unprocessable_entity
      return
    end

    @account_title = AccountTitle.where('(book_id IS NULL OR book_id = ?) AND aggregate IS FALSE AND id = ?', @book.id, @term.ac_id).take
    raise "internal error" if !@account_title
    division = @term.div_id.to_i > 0 ?
                 Division.where('(book_id IS NULL OR book_id = ?) AND aggregate IS FALSE AND id = ?', @book.id, @term.div_id).take
                 : nil

    @period = PeriodEnd.get_period(@book, @term.from)
    beginning = @account_title.ac_type == 1 ? nil : @period.beginning

    gl = GeneralLedger.new(@book, @account_title)
    @balance_amt = gl.get_balance(beginning, @term.from - 1, division)
    @entries = JournalEntry.where('book_id = ? AND (date BETWEEN ? AND ?) AND account_id = ?', @book.id, @term.from, @term.to, @account_title.id)
                 .order(:date, :entry_no)
    @entries = @entries.where('division_id = ?', division.id) if division
  end

  
private
  # Only allow a list of trusted parameters through.
  def general_ledger_params
    params.fetch(:general_ledger, {})
  end
end
