
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class Connect::GoogleOauth2Controller < ApplicationController
  skip_before_action :require_login #, only:%i[login]
  
  # POST /auth/google_oauth2 認証開始
  def create
    session[:state] = SecureRandom.hex(32)
    session[:nonce] = SecureRandom.hex(32)
    redirect_to Connect::GoogleIdp.authorization_uri(
                    response_type: 'code',     # Authorization Code Flow
                    scope: ['openid', "email", "profile"], # 'openid' 必須
                    state: session[:state],    # 推奨
                    nonce: session[:nonce] ),  # 推奨. FAPI では必須
                allow_other_host: true,
                status: :see_other
  end

  # IdP からリダイレクトバック
  def show
    if params[:code].blank? || session[:state] != params[:state]
      raise AuthenticationRequired.new
    end
    session.delete(:state)

    # login() 内部で user_class.authenticate(*credentials) が呼び出される
    nonce = session[:nonce]; session.delete(:nonce)
    if login(Connect::GoogleIdp, params[:code], nonce) 
      redirect_back_or_to('/books/', notice: 'Login successful')
    else
      flash[:alert] = 'Login failed'
      redirect_to '/'
    end
  end
end
