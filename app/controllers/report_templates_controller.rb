
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 報告様式マスタ
class ReportTemplatesController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book

  before_action :set_report, only: %i[ show edit update destroy ]

  
  # GET /reports or /reports.json
  def index
    @reports = Report.where('book_id IS NULL OR book_id = ?', @book.id)
  end


  # GET /reports/1 or /reports/1.json
  def show
    @reporting_titles = ReportingTitle.where('report_id = ?', @report.id).order('position')

    # form
    @reporting_title = ReportingTitle.new report_id:@report.id
    @ac_list = AccountTitle.grouped_items(@book, false)
  end


  # GET /reports/new
  def new
    @report = Report.new
  end

  
  # POST /reports or /reports.json
  def create
    @report = Report.new(report_params)
    @report.book_id = @book.id
    if @report.save
      redirect_to( {action:'show', id:@report},
                   notice: "Report was successfully created." )
    else
      render :new, status: :unprocessable_entity 
    end
  end


  # GET /reports/1/edit
  def edit
  end

  
  # PATCH/PUT /reports/1 or /reports/1.json
  def update
    if @report.update(report_params)
      redirect_to( {action:'show', id:@report},
                   notice: "Report was successfully updated." )
    else
      render :edit, status: :unprocessable_entity 
    end
  end

  
  # DELETE /reports/1 or /reports/1.json
  def destroy
    @report.destroy
    redirect_to book_report_templates_path, notice: "Report was successfully destroyed." 
  end

  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_report
    @report = Report.where('(book_id IS NULL OR book_id = ?) AND id = ?', @book.id, params[:id]).take
    raise ActiveRecord::RecordNotFound if !@report
  end

  # Only allow a list of trusted parameters through.
  def report_params
    params.require(:report).permit(:name, :description)
  end
end
