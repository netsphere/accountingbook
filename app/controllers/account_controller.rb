# -*- coding:utf-8 -*-

# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# ユーザ認証関係
class AccountController < ApplicationController

  skip_before_action :require_login,
        only: [:sign_in, :signup, :signup2, :confirm_signup, ]

=begin
  #########################################################################
  # OpenID 設定
  ROOT_CERTIFICATES_FILE = "/etc/pki/tls/cert.pem"
=end


  def index
#if ENABLE_SSO
#    redirect_to AccountingBook::Application.config.accounts_app_url + "/account"
#end # if ENABLE_SSO
  end

  
  # 利用規約確認 (画面)
  def signup; end


  # 仮登録 (画面)
  def signup2
    if params[:un].blank?
      @error = "チェックが入っていません。"
      render :action => "signup"
      return
    end

    if session[:invitation]
      # メールアドレスは確認ずみ. 直接ユーザ登録へ
      redirect_to :controller => "account", :action => "new", 
                         :token => session[:invitation].email.confirm_token
      return
    end
  end


  # 仮登録 (メール送信)
  def confirm_signup
    Email.sweep

    # すでに正式登録されている場合は、送信しない。（メッセージは区別しない。）
    e = Email.find_by_email_addr params[:email][:email_addr]
    if !( e && !e.confirm_token )
      @email = e || (Email.new params[:email], :without_protection => true)
      @email.email_addr = params[:email][:email_addr]
      @email.attributes = {
        :can_login => true,
        :confirm_token => User.encrypt(rand(2 ** 32).to_s + rand(2 ** 32).to_s,
                                       Time.now.to_s),
        :confirm_sent_at => Time.now
      }

      mail = AccountMailer.signup @email
      raise TypeError, mail.inspect if !mail.is_a?(Mail::Message) # DEBUG

      begin
        @email.save!  # ここでメールアドレスの妥当性検査
        mail.deliver
      rescue ActiveRecord::RecordInvalid
        render :action => "signup2"
        return
      end
    end

    flash[:notice] = "確認メールを送信しました。メールに記載のURLをクリックして、ユーザ登録を続けてください。"
    redirect_to :controller => "welcome", :action => "index"
  end


  # GET
  def sign_in
    #set_current_user ::User.find 2268  # DEBUG DEBUG DEBUG
    #redirect_to :controller => 'welcome', :action => 'index'
    #render layout:false
  end


  def sign_out
    raise if !request.post?

    #current_user.forget_me!
    #set_current_user nil
    #request.session_options[:renew] = true
    logout() # sorcery
    
    # ● TODO: IdP のほうもログアウトさせるべき.
    #
    # Azure AD はサポートしている:
    # https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration
    # "http_logout_supported": true,
    #     --> OpenID Connect HTTP-Based Logout 1.0
    # "frontchannel_logout_supported": true,
    #     --> OpenID Connect Front-Channel Logout 1.0
    # "end_session_endpoint": "https://login.microsoftonline.com/common/oauth2/v2.0/logout",
    
    flash[:notice] = "ログアウトしました。"
    redirect_to :controller => "welcome", :action => "index"
  end

end # of AccountController
