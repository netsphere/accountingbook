
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 出納帳. 単数形リソース
class CashbooksController < ApplicationController
  before_action :set_cash_book, only: %i[ show edit update destroy ]

  # GET /cash_books or /cash_books.json
  def index
    @cash_books = CashBook.all
  end

  # GET /cash_books/1 or /cash_books/1.json
  def show
  end

  # GET /cash_books/new
  def new
    @cash_book = CashBook.new
  end

  # GET /cash_books/1/edit
  def edit
  end

  # POST /cash_books or /cash_books.json
  def create
    @cash_book = CashBook.new(cash_book_params)

    respond_to do |format|
      if @cash_book.save
        format.html { redirect_to @cash_book, notice: "Cash book was successfully created." }
        format.json { render :show, status: :created, location: @cash_book }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @cash_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cash_books/1 or /cash_books/1.json
  def update
    respond_to do |format|
      if @cash_book.update(cash_book_params)
        format.html { redirect_to @cash_book, notice: "Cash book was successfully updated." }
        format.json { render :show, status: :ok, location: @cash_book }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @cash_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cash_books/1 or /cash_books/1.json
  def destroy
    @cash_book.destroy
    respond_to do |format|
      format.html { redirect_to cash_books_url, notice: "Cash book was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cash_book
      @cash_book = CashBook.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cash_book_params
      params.fetch(:cash_book, {})
    end
end
