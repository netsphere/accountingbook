
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 期間選択
class JournalsTerm
  include ActiveModel::Validations
  include ApplicationHelper
  
  attr_accessor :from, :to

  validate :check

  def initialize params = {}
    @from = date_from_str params[:from]
    @to =   date_from_str params[:to]
  end

  # @override Object#to_hash
  def to_hash
    {from:@from, to:@to}
  end
  
private
  # for `validate()`
  def check
    errors.add(:from, "cannot be nil") if from.blank?
    errors.add(:to,   "cannot be nil") if to.blank?
    errors.add(:to, "must be from <= to") if from > to
  end
end


# 仕訳帳・振替伝票
# URL の `:id` は `entry_no`. `(date, entry_no)` が主キー.
class JournalsController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book

  before_action :set_journal, only: %i[ show edit update destroy ]


  # GET /journals or /journals.json
  def index
    # ●●TODO: 締め日の次の日を開始日にするのがよい。
    today = Date.today
    @term = JournalsTerm.new(from: Date.new(today.year, today.mon, 1),
                             to:   Date.new(today.year, today.mon, 1).next_month - 1)
  end


  # 仕訳帳
  def list
    @term = JournalsTerm.new params[:journals_term]
    if !@term.valid?
      render 'index', status: :unprocessable_entity
      return
    end

    @journals = JournalEntry.where('book_id = ? AND date BETWEEN ? AND ?',
                                   @book.id, @term.from, @term.to)
                            .order("date, entry_no, id")
  end


  # GET /journals/1 or /journals/1.json
  def show
    # ● TODO: impl.
  end


  # 新しい振替伝票
  # GET /journals/new
  def new
    date = Date.today
    @term = JournalsTerm.new(params[:journals_term]) if !params[:journals_term].blank?
    @journal = Journal.new date:date, entries:[JournalEntry.new]
  end


  
  # POST
  def add_line
    @journal = Journal.new( date: Date.parse(params[:date]) )
    entry = JournalEntry.new
    lineno = params[:lineno]
    new_lineno = rand(200_000_000)
    render( turbo_stream: turbo_stream.before("entries_#{lineno}",
                                    partial: "form_entry",
                                    locals: {entry:entry, lineno:new_lineno}) )
  end


  # POST /journals or /journals.json
  def create
    #date = Date.parse(params[:journal][:date])

    begin
      @journal = Journal.create_from_params(@book, journal_params.permit(:date),
                                            params[:entries] )
      JournalEntry.transaction do
        @journal.entries.each do |entry|
          entry.create_user_code = current_user.login
          entry.update_user_code = current_user.login
          entry.save!
        end
      end
    rescue ActiveRecord::RecordInvalid
      render :new, status: :unprocessable_entity
      return
    end
    
    redirect_to( {action:'show', id:@journal.entries.first.entry_no,
                  date:@journal.date},
                 notice: "Journal was successfully created." )
  end


  # GET /journals/1/edit
  def edit
  end


  # PATCH/PUT /journals/1 or /journals/1.json
  def update
    orig_entry = @journal.entries.first
    
    # 全部を差し替える
    @journal = Journal.create_from_params(@book, journal_params.permit(:date),
                                          params[:entries] )

    JournalEntry.transaction do
      # 先に消す
      JournalEntry.where('book_id = ? AND date = ? AND entry_no = ?',
                         @book.id, orig_entry.date, orig_entry.entry_no).delete_all
      @journal.entries.each do |entry|
        entry.entry_no = orig_entry.entry_no
        entry.create_user_code = orig_entry.create_user_code
        entry.update_user_code = current_user.login
        entry.save!
      end
    rescue ActiveRecord::RecordInvalid
      render :edit, status: :unprocessable_entity
      return
    end
    
    redirect_to({action:'show', id:@journal.entries.first.entry_no,
                 date:@journal.date},
                notice: "Journal was successfully updated." )
  end


  # DELETE /journals/1 or /journals/1.json
  def destroy
    entry = @journal.entries.first
    JournalEntry.where('book_id = ? AND date = ? AND entry_no = ?',
                       @book.id, entry.date, entry.entry_no).delete_all

    redirect_to( {action:'index'},
                 notice: "Journal was successfully destroyed." )
  end


private ##################################

  # Use callbacks to share common setup or constraints between actions.
  def set_journal
    entry_no = params[:id]
    date = Date.parse(params[:date])
    @journal = Journal.new(date:date,
                           entry_no:entry_no,
                           entries: JournalEntry.where('book_id = ? AND date = ? AND entry_no = ?', @book.id, date, entry_no) )
  end


  # Only allow a list of trusted parameters through.
  def journal_params
    params.require(:journal)
  end
end
