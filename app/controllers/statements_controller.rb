
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class StatementTerm
  include ActiveModel::Validations
  include ApplicationHelper

  # テンプレート
  attr_reader :report_id

  # 自
  attr_reader :from
  # 至
  attr_reader :to

  validates_presence_of :from
  validates_presence_of :to
  
  validates_presence_of :report_id
  validate :check
  
  def initialize params = {}
    @from      = date_from_str params[:from]
    @to        = date_from_str params[:to]
    @report_id = params[:report_id]
  end

  
private
  # for `validate()`
  def check
    errors.add(:to, "must be from <= to") if from > to
  end
  
end


# 財務諸表. 単数形リソース. G/L と同じつくり
# 「財務諸表」のビジネスロジックは, Statement クラスに格納する.
class StatementsController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book
  
  
  # GET /statements or /statements.json
  def show
    today = Date.today
    @term = StatementTerm.new(
                    from:Date.new(today.year, today.mon, 1),
                    to:  Date.new(today.year, today.mon, 1).next_month - 1)
  end

  # GET
  def list
    @term = StatementTerm.new params[:statement_term]
    if !@term.valid?
      render 'show', status: :unprocessable_entity
      return
    end

    # 1. 全部の実在科目の期首残高を得る。発生科目の累計金額も必要
    stmt = Statement.new(@book)
    @period = PeriodEnd.get_period(@book, @term.from)
    @balance_amt, @amts_arising = stmt.make_cube(
                                        Report.find(@term.report_id),
                                        @period.beginning,
                                        @term.from, @term.to,
                                        nil) # TODO: division
  end


private
  # Only allow a list of trusted parameters through.
  def statement_params
    params.fetch(:statement, {})
  end
end
