
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 資金口座マスタ・クレジットカード (経費精算のほうで相手勘定にするため)
#   -> 振込依頼データを作るときに、初めて必要になる。
class CashAccountsController < ApplicationController
  before_action :set_cash_account, only: %i[ show edit update destroy ]

  # GET /cash_accounts or /cash_accounts.json
  def index
    @cash_accounts = CashAccount.all
  end

  # GET /cash_accounts/1 or /cash_accounts/1.json
  def show
  end

  # GET /cash_accounts/new
  def new
    @cash_account = CashAccount.new
  end

  # GET /cash_accounts/1/edit
  def edit
  end

  # POST /cash_accounts or /cash_accounts.json
  def create
    @cash_account = CashAccount.new(cash_account_params)

    respond_to do |format|
      if @cash_account.save
        format.html { redirect_to @cash_account, notice: "Cash account was successfully created." }
        format.json { render :show, status: :created, location: @cash_account }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @cash_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cash_accounts/1 or /cash_accounts/1.json
  def update
    respond_to do |format|
      if @cash_account.update(cash_account_params)
        format.html { redirect_to @cash_account, notice: "Cash account was successfully updated." }
        format.json { render :show, status: :ok, location: @cash_account }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @cash_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cash_accounts/1 or /cash_accounts/1.json
  def destroy
    @cash_account.destroy
    respond_to do |format|
      format.html { redirect_to cash_accounts_url, notice: "Cash account was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cash_account
      @cash_account = CashAccount.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cash_account_params
      params.fetch(:cash_account, {})
    end
end
