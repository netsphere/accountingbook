
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class ApplicationController < ActionController::Base
  # Only allow modern browsers supporting webp images, web push, badges, import maps, CSS nesting, and CSS :has.
  allow_browser versions: :modern

  # 自動で読み込まれる。`include` 不要.
  #include ApplicationHelper
  
  # Sorcery core module
  # ポカ避けのため、ログイン不要なコントローラで `skip_before_action` すること.
  before_action :require_login

  # Pagination
  include Pagy::Backend


private
  
  # Sorcery: 未ログインの状態で要ログインのページにアクセスした時に呼び出される
  def not_authenticated
    flash[:alert] = "ログインしてください。"
    redirect_to sign_in_account_path() #"/account/sign_in"
  end


  # for `before_action`
  def check_if_user_can_access_book
    @book = Book.find( controller_name == "books" ?
                                      params[:id] : params[:book_id] )
    #raise ActiveRecord::RecordNotFound if !@book

    if !@book || !@book.member?(current_user)
      render text: "Forbidden to access this book.", status: 403
    end
  end

end
