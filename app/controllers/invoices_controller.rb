
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# (購買)請求書
class InvoicesController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book

  before_action :set_invoice, only: %i[ show edit update destroy ]


  # GET /invoices
  # GET /invoices.json
  def index
    @pagy, @invoices = pagy(Invoice.where("book_id = ?", @book.id).order('date'))
  end


  # GET /invoices/1
  # GET /invoices/1.json
  def show
  end


  # GET /invoices/new
  # GET /invoices/new.json
  def new
    @invoice = Invoice.new
    @invoice_details = []
  end


  # GET /invoices/1/edit
  def edit
    @invoice_details = InvoiceDetail.find :all,
                            :conditions => ["invoice_id = ?", @invoice.id],
                            :order => "lineno, id"
  end


  # POST /invoices
  # POST /invoices.json
  def create
    @invoice = Invoice.new params.require(:invoice).permit(:partner_id, :date)
    @invoice.book_id = @book.id
    @invoice.create_user_code = current_user.login

    begin
      Invoice.transaction do 
        amount_total = 0
        details = []
        (0 .. params[:lines].to_i - 1).each do |i|
          detail = InvoiceDetail.new params.require("detail#{i}") \
                        .permit(:lineno, :account_title_id, :amount, :remarks)
          if detail.amount.to_i != 0
            details << detail
            amount_total += detail.amount.to_i
          end
        end

        @invoice.amount_total = amount_total
        @invoice.save!
        details.each do |detail|
          detail.invoice_id = @invoice.id
          detail.save!
        end

        # 総勘定元帳に転記
        entry_no = (@book.setting.entry_no_counter += 1)
        @book.setting.save!
        partner = @invoice.partner
        details.each do |detail|
          je = JournalEntry.new :book_id => @book.id,
                    :date => [@invoice.date, @book.setting.fixed_date + 1].max,
                    :entry_no => entry_no,
                    :dr_account_id => detail.account_title_id,
                    :dr_partner_id => nil,
                    :cr_account_id => @book.setting.payable_item,
                    :cr_partner_id => partner.id,
                    :amount => detail.amount,
                    :remarks => partner.name,
                    :comment => detail.remarks,
                    :invoice_id => @invoice.id # 転記元
          je.save!
        end
      end # transaction
    rescue ActiveRecord::RecordInvalid
      render :action => "new"
      return
    end

    redirect_to( {:action => "show", :id => @invoice}, 
                 :notice => 'Invoice was successfully created.' )
  end


  # PUT /invoices/1
  # PUT /invoices/1.json
  def update
    @invoice = Invoice.find(params[:id])

    respond_to do |format|
      if @invoice.update_attributes(params[:invoice])
        format.html { redirect_to @invoice, :notice => 'Invoice was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @invoice.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /invoices/1
  # DELETE /invoices/1.json
  def destroy
    @invoice = Invoice.find(params[:id])
    @invoice.destroy

    respond_to do |format|
      format.html { redirect_to invoices_url }
      format.json { head :no_content }
    end
  end


private
  # Use callbacks to share common setup or constraints between actions.
  def set_invoice
    @invoice = Invoice.where("id = ? AND book_id = ?", params[:id], @book.id) \
                      .first
    raise if !@invoice
  end

  # Only allow a list of trusted parameters through.
  def invoice_params
    params.fetch(:invoice, {})
  end
end
