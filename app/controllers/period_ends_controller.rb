
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 会計期間
class PeriodEndsController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book
  
  before_action :set_period_end, only: %i[ edit update destroy ]

  
  # GET /period_ends or /period_ends.json
  def index
    @period_ends = PeriodEnd.where('book_id = ?', @book.id).order('date')
  end

=begin
  # GET /period_ends/1 or /period_ends/1.json
  def show
  end
=end
  
  # GET /period_ends/new
  def new
    @period_end = PeriodEnd.new
  end


  # POST /period_ends or /period_ends.json
  def create
    @period_end = PeriodEnd.new(period_end_params)
    @period_end.book_id = @book.id
    @period_end.create_user_code = current_user.login
      
    if @period_end.save
      redirect_to( {action:'index'},
                   notice: "Period end was successfully created." )
    else
      render :new, status: :unprocessable_entity 
    end
  end

  
  # GET /period_ends/1/edit
  def edit
  end

  # PATCH/PUT /period_ends/1 or /period_ends/1.json
  def update
    if @period_end.update(period_end_params)
      redirect_to({action:'index'},
                  notice: "Period end was successfully updated." )
    else
      render :edit, status: :unprocessable_entity 
    end
  end

  
  # DELETE /period_ends/1 or /period_ends/1.json
  def destroy
    @period_end.destroy
    redirect_to({action:'index'},
                notice: "Period end was successfully destroyed." )
  end


private #####################################################
  
  # Use callbacks to share common setup or constraints between actions.
  def set_period_end
    @period_end = PeriodEnd.where('book_id = ? AND id = ?', @book.id, params[:id]).take
    raise ActiveRecord::RecordNotFound if !@period_end
  end

  # Only allow a list of trusted parameters through.
  def period_end_params
    params.require('period_end').permit(:date, :vat_taxable, :vat_exclusive, :registration_of_taxable, :description)
  end
  
end # class PeriodEndsController
