
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class PartnerSearch
  include ActiveModel::Validations
  include ApplicationHelper

  attr_reader :text

  validates_presence_of :text

  def initialize params = {}
    @text = params[:text]
  end

  def find_by_text
    return nil if @text.to_s.strip == ''
    
    t = @text.unicode_normalize(:nfkc).strip.upcase 
    case
    when (t[0].eql?('|') && !t[t.length-1].eql?('|')) #前方検索
      t.slice!(0) ; t += '%'
    when (t[t.length-1].eql?('|') && !t[0].eql?('|')) #後方検索
      t = '%' + t.chop!
    else #部分検索
      t = '%' + t.strip + '%'
    end
    return Partner.where('upper(name) LIKE ? OR kana_name LIKE ? OR upper(description) LIKE ?', t, t, t)
                    .order("name")
                    .limit(Partner::PAGINATE_MAX)
  end
end


# 取引先マスタ
class PartnersController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book
  
  before_action :set_partner, only: %i[ show edit update destroy ]


  # GET /partners
  # GET /partners.json
  def index
    @search = PartnerSearch.new
  end


  def search
    @search = PartnerSearch.new params[:partner_search]
    #raise @search.inspect
    if !@search.valid?
      render 'index', status: :unprocessable_entity
      return
    end
    
    @pagy, @partners = pagy(@search.find_by_text)
  end
  
  # GET /partners/1
  # GET /partners/1.json
  def show
  end


  # GET /partners/new
  # GET /partners/new.json
  def new
    @partner = Partner.new
    @partner.active = true
  end


  # GET /partners/1/edit
  def edit
  end


  # POST /partners
  # POST /partners.json
  def create
    @partner = Partner.new( partner_params )
    @partner.book_id = @book.id
    @partner.create_user_code = current_user.login
    @partner.update_user_code = current_user.login
    
    begin
      ActiveRecord::Base.transaction do
        @partner.save!
      end
      redirect_to( {:action => "show", :id => @partner}, 
                  :notice => 'Partner was successfully created.' )
    rescue ActiveRecord::RecordInvalid
      render :new, status: :unprocessable_entity
    end
  end


  # PUT /partners/1
  # PUT /partners/1.json
  def update
    @partner.attributes = partner_params
    begin
      ActiveRecord::Base.transaction do
        @partner.save!
      end
      redirect_to( {:action => "show", :id => @partner}, 
                   :notice => 'Partner was successfully updated.' )
    rescue ActiveRecord::RecordInvalid
      render :edit, status: :unprocessable_entity 
    end
  end


  # DELETE /partners/1
  # DELETE /partners/1.json
  def destroy
    @partner.destroy
    redirect_to( {:action => "index"}, :notice => "取引先を削除しました。" )
  end


private
  # Use callbacks to share common setup or constraints between actions.
  def set_partner
    @partner = Partner.where("id = ? AND book_id = ?", params[:id], @book.id) \
                      .first
    raise if !@partner
  end

  # Only allow a list of trusted parameters through.
  def partner_params
    params.require(:partner).permit(:active, :name, :kana_name, :description)
  end

end # class PartnersController
