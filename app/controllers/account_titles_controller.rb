
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 勘定科目マスタ
class AccountTitlesController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book

  before_action :set_account_title, only: %i[ show edit update destroy enable_ac]

  # GET /account_titles or /account_titles.json
  # `params[:tab]` root 科目
  def index
    # System 科目は未利用でも表示.
    roots = AccountTitle.tree_roots(@book)
    @tab = (roots.find do |x| x.id == params[:tab].to_i end) || roots.first
    @ac_sections = roots.map do |root|
      {name: root.name, link: root.id, active: root.id == @tab.id}
    end

    r = { id: @tab.id, name: @tab.name, aggregate: @tab.aggregate,
          show_link: "/books/#{@book.id}/account_titles/#{@tab.id}",
          sort: @tab.sort, active: true, "_children": [] }
    r["_children"] = @tab.children_json(@book, @tab)
    @ac_tab_ary = [r]
  end


  # 勘定科目の詳細
  # GET /account_titles/1 or /account_titles/1.json
  def show
  end


  # 新しい勘定科目 (画面)
  # GET /account_titles/new
  def new
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user ? true : false
    
    @account_title = AccountTitle.new
    @parent_id = nil
    @active = true
  end


  # POST /account_titles or /account_titles.json
  def create
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user ? params[:system_item] : false
    
    @account_title = AccountTitle.new(account_title_params.permit(
                        %i[aggregate sort name name_short description sign kind ac_type enable_invoice payable_id tax_group]) )
    @active = params[:active] || false
    
    if !params[:parent_id].blank?
      parent = AccountTitle #.left_outer_joins(:ac_activations)
                   .where('(book_id IS NULL or book_id = ?) AND aggregate IS TRUE AND account_titles.id = ?', @book.id, params[:parent_id])
                   .take
    end

    @account_title.book_id = @book.id if !@system_item
    #@account_title.suppl_code = 0
    
    # 実在科目を root に置くのは不可
    if !@account_title.aggregate && !parent
      @account_title.errors.add :base, "実在科目は root に置けません。"
      render :new, status: :unprocessable_entity
      return
    end
    
    begin
      ActiveRecord::Base.transaction do
        @account_title.save!
        if parent
          parent.add_child(@account_title,
                           main_parent:true, weight:1)
        end

        if !@system_item
          AcOvrd.create! account_title_id: @account_title.id,
                         book_id: @book.id, active:@active,
                         name: @account_title.name,
                         name_short: @account_title.name_short,
                         sign: @account_title.sign
        end
      end
    rescue ActiveRecord::RecordInvalid
      @parent_id = parent.id if parent
      render :new, status: :unprocessable_entity
      return
    end
    
    redirect_to( {action:'show', id: @account_title.id},
                 notice: "Account title was successfully created." )
  end

  
  # GET /account_titles/1/edit
  def edit
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user && !@account_title.book_id ? true : false

    ovrd = @account_title.ac_ovrd(@book)
    if ovrd
      # `ovrd.attributes` だと unknown attribute 'account_title_id' for AccountTitle
      # -> 手書きする
      @account_title.assign_attributes(
        name: ovrd.name, name_short:ovrd.name_short, sign:ovrd.sign )
      @active = ovrd.active
    end
  end


  # PATCH/PUT /account_titles/1 or /account_titles/1.json
  def update
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user ? params[:system_item] : false

    @active = params[:active] || false  # `|| false` を付けないと nil になる

    # ここでは保存しない
    # `aggregate` 後から変更不可.
    @account_title.assign_attributes(account_title_params.permit(
                                       %i[sort name name_short description sign kind ac_type enable_invoice payable_id tax_group]) )
    
    if @system_item
      ovrd = @account_title.ac_ovrd(@book)
    else
      # いつでも ovrd をつくる
      ovrd = @account_title.ac_ovrd(@book) || AcOvrd.new(
               account_title_id: @account_title.id, book_id: @book.id)
      ovrd.assign_attributes(
               active: @active, name: @account_title.name,
               name_short:@account_title.name_short, sign: @account_title.sign )
    end

    ActiveRecord::Base.transaction do
      if @system_item || @account_title.book_id
        @account_title.save!
      end
      if !@system_item
        ovrd.save!
      else
        ovrd.delete if ovrd
      end
    rescue ActiveRecord::RecordInvalid
      @parent_id = @account_title.main_parent ? @account_title.main_parent.id : nil
      render :edit, status: :unprocessable_entity 
      return
    end
        
    redirect_to( {action:'show', id:@account_title},
                 notice: "Account title was successfully updated." )
  end

  
  # 勘定科目を利用する: book ごと
  # POST
  def enable_ac
    ovrd = AcOvrd.find_or_initialize_by(
             book_id: @book.id, account_title_id: @account_title.id) do |m|
      m.active     = true
      m.name       = @account_title.name
      m.name_short = @account_title.name_short
      m.sign       = @account_title.sign
    end
    ovrd.save!

    index() # ary を更新
    # turbo stream で返す
    render( turbo_stream: turbo_stream.replace("ac_table_frame", partial:'ac_table_frame'))
  end


  # DELETE /account_titles/1 or /account_titles/1.json
  def destroy
    if !@account_title.book_id # system_item
      flash[:alert] = "システム科目は削除できません"
      render :show, status: :unprocessable_entity
      return
    end
    
    ActiveRecord::Base.transaction do
      # `where()` して消すのは `delete_all()`
      AcOvrd.where('book_id = ? AND account_title_id = ?', @book.id, @account_title.id).delete_all
        
        # 途中の場合, 間を詰めないといけない (`distance` を減らす)
        # @note 生 SQL の書き方が `execute()` ばかり引っ掛かるが、安全でない
        #       正しいメソッドは `find_by_sql()`.
        #       `SELECT` 文を書きたい場合は `select()`, `from()`
      AccountTitleTree.find_by_sql [<<EOS, @account_title.id, @account_title.id, @account_title.id, @account_title.id]
UPDATE account_title_trees
    SET distance = distance - 1
    WHERE
        parent_id IN (SELECT parent_id FROM account_title_trees
                                       WHERE child_id = ? AND parent_id <> ?)
        AND child_id IN (SELECT child_id FROM account_title_trees
                                         WHERE parent_id = ? AND child_id <> ?)
EOS
        
      # あとは削除するだけ
      AccountTitleTree.where('parent_id = ? OR child_id = ?',
                               @account_title.id, @account_title.id).delete_all
      @account_title.destroy!
    end
      
    redirect_to( {controller:"account_titles", action:"index", book_id:@book.id},
                 notice: "Account title was successfully destroyed." )
  end


private #############################################
  
  # Use callbacks to share common setup or constraints between actions.
  def set_account_title
    @account_title = AccountTitle.where('(book_id IS NULL or book_id = ?) AND id = ?', @book.id, params[:id]).take
    raise ActiveRecord::RecordNotFound if !@account_title
  end

  # Only allow a list of trusted parameters through.
  def account_title_params
    params.require(:account_title)
  end

end # class AccountTitlesController 
