
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 会計部門マスタ
class DivisionsController < ApplicationController
  # `@book` の設定を兼ねる
  before_action :check_if_user_can_access_book

  before_action :set_division, only: %i[ show edit update destroy enable_div]

  
  # GET /divisions or /divisions.json
  def index
    # System 部門は未利用でも表示.
    #@pagy, @divisions = pagy(
    #            Division.where('book_id IS NULL or book_id = ?', @book.id)
    #                    .order('division_code') )

    @div_ary = []
    Division.tree_roots(@book).each do |root_div|
      r = { id: root_div.id, name: root_div.name,
            aggregate:root_div.aggregate,
            division_code: root_div.division_code,
            active: true,
            "_children": [] }
      r["_children"] = root_div.children_json(@book)
      @div_ary << r
    end
  end

  # POST
  def enable_div
    ovrd = DivOvrd.find_or_initialize_by(
                book_id: @book.id, division_id: @division.id) do |m|
      m.end_day = nil
    end
    ovrd.save!

    index()  # @div_ary を更新
    # turbo frame だと上手くいかない. turbo stream で返す
    render(turbo_stream: turbo_stream.replace("div-frame", partial:"div_frame"))
  end
 
  
  # GET /divisions/1 or /divisions/1.json
  def show
  end

  # GET /divisions/new
  def new
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user ? true : false
    
    @division = Division.new
  end

  
  # POST /divisions or /divisions.json
  def create
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user ? params[:system_item] : false
    
    @division = Division.new(division_params.permit(
                               %i[aggregate division_code name description purchase_active vat_disable]))
    @end_day  = params[:end_day] 

    if !params[:parent_id].blank?
      parent = Division.where('(book_id IS NULL or book_id = ?) AND aggregate IS TRUE AND divisions.id = ?', @book.id, params[:parent_id])
                       .take
    end

    @division.book_id = @book.id  if !@system_item
      
    # 実在部門を root に置くのは不可
    if !@division.aggregate && !parent
      @division.errors.add :base, "実在部門は root に置けません。"
      render :new, status: :unprocessable_entity
      return
    end

    begin
      ActiveRecord::Base.transaction do
        @division.save!
        if parent
          parent.add_child(@division, main_parent:true)
        end

        if !@system_item
          DivOvrd.create! division_id: @division.id, book_id: @book.id,
                          end_day: @end_day
        end
      end
    rescue ActiveRecord::RecordInvalid
      @parent_id = parent.id if parent
      render :new, status: :unprocessable_entity
      return
    end
    
    redirect_to( {action:'show', id:@division.id},
                 notice: "Division was successfully created." )
  end


  # GET /divisions/1/edit
  def edit
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user && !@division.book_id ? true : false
    
    ovrd = @division.div_ovrd(@book)
    @end_day = ovrd ? ovrd.end_day : nil

    @parent_id = @division.main_parent ? @division.main_parent.id : nil
  end

  
  # PATCH/PUT /divisions/1 or /divisions/1.json
  def update
    @admin_user = current_user  # ●● TODO: fix!
    @system_item = @admin_user ? params[:system_item] : false

    @end_day = params[:end_day]

    # ここでは保存しない。
    vat_disable = @division.vat_disable
    @division.assign_attributes(division_params.permit(
                                    %i[division_code name description purchase_active vat_disable]))
    @division.vat_disable = true if vat_disable  # disable から変更不可. TODO: ovrd にもフィールドつくるか?

    if @system_item
      ovrd = @division.div_ovrd(@book)
    else
      # いつでも ovrd をつくる
      ovrd = @division.div_ovrd(@book) || DivOvrd.new(division_id: @division.id, book_id: @book.id)
      ovrd.end_day = @end_day
    end

    ActiveRecord::Base.transaction do
      if @system_item || @division.book_id
        @division.save!
      end
      if !@system_item
        ovrd.save!
      else
        ovrd.destroy! if ovrd
      end
    rescue ActiveRecord::RecordInvalid
      @parent_id = @division.main_parent ? @division.main_parent.id : nil
      render :edit, status: :unprocessable_entity 
      return
    end
    
    redirect_to({action:'show', id:@division},
                  notice: "Division was successfully updated." )
  end

  
  # DELETE /divisions/1 or /divisions/1.json
  def destroy
    @division.destroy!

    redirect_to( {action:'index'},
                 notice: "Division was successfully destroyed." )
  end

  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_division
    @division = Division.where('(book_id IS NULL or book_id = ?) AND id = ?',
                               @book.id, params[:id]).take
    raise ActiveRecord::RecordNotFound if !@division
  end

  # Only allow a list of trusted parameters through.
  def division_params
    params.require(:division)
  end
end
