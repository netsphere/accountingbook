json.extract! cc_reconciliation, :id, :created_at, :updated_at
json.url cc_reconciliation_url(cc_reconciliation, format: :json)
