
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# ユーザグループ. 裏で自動的に作る
class Space < ActiveRecord::Base
  has_many :spaces_users
  has_many :users, through: :spaces_users

  has_many :books
  
  validates_uniqueness_of :urlkey
  validates_presence_of :urlkey

  validates_presence_of :name

  
  def member? user
    return false if !user
    if user.class.to_s != ::User.to_s
      raise TypeError, "expected ::User, but '#{user.class}'" 
    end

    g = SpacesUser.joins("JOIN users ON spaces_users.user_id = users.id") \
                  .where("users.login = ? AND space_id = ?", 
                                        user.login, self.id).first
    !g.nil?
  end

  
  # @return [Array of String] space の urlkeyの一覧
  def self.accessable_urlkeys user
    raise TypeError if !user
    #raise TypeError, "expected Accounts::User, but #{user.class}" if user.class != Accounts::User

    list = self.joins("JOIN spaces_users ON spaces_users.space_id = spaces.id") \
               .where("user_id = ?", user.id)
    return (list.map do |r| r.urlkey end)
  end

end # class Space
