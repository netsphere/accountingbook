
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 会計帳簿. アプリケィションの中心
class Book < ApplicationRecord

  belongs_to :created_by, :foreign_key => "create_user_code",
             :class_name => "User", :primary_key => "login"
  belongs_to :updated_by, :foreign_key => "update_user_code",
             :class_name => "User", :primary_key => "login"

  # 直接ユーザと繋がっていない
  #belongs_to :space, foreign_key:'user_group_code', primary_key:'urlkey'
  has_many :books_users
  
  has_one :setting, :class_name => "BookSetting"

  # 仕訳明細
  has_many :journal_entries

  # PK
  #validates_uniqueness_of :urlkey  廃止。`id` を文字列にする.
  #validates_presence_of   :urlkey

  validates_presence_of :name

  before_create :set_id

  
  def member? user
    return false if !user
    if user.class.to_s != ::User.to_s
      raise TypeError, "expected ::User, but '#{user.class}'" 
    end

    g = BooksUser.where("book_id = ? AND user_id = ?", self.id, user.id).first
    return !g.nil?
  end

  
private

  # for `before_create()`
  def set_id
    # `alphanumeric()` は 62種類の文字を使う (5.95bit分). 64bit は 10.8 文字でよい
    self.id ||= SecureRandom.alphanumeric(12)
  end
  
end
