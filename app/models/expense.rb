
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 領収書にもとづく経費精算
# 支払い (照合) は cc_reconciliations table.
class Expense < ApplicationRecord
  # attr_accessible :title, :body

  # 親
  belongs_to :book

  # 資金科目・クレジットカード
  belongs_to :cash_account

  belongs_to :created_by, :foreign_key => "create_user_code",
                          :class_name => "User", :primary_key => "login"
  belongs_to :updated_by, :foreign_key => "update_user_code",
                          :class_name => "User", :primary_key => "login"

end
