
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 元帳の残高と発生額
# DB ではない
class GeneralLedger
  def initialize(book, ac)
    raise TypeError if !book.is_a?(Book)
    raise TypeError if !ac.is_a?(AccountTitle)
    @book = book
    @ac = ac
  end

  
  # 指定期間の発生金額
  # @param b, e  開始, 終了。いずれも含む
  # @return [BigDecimal] 金額
  #
  # accrue (v.) がプラス, incur (v.) がマイナス. 単に発生は arise (vi.)
  # 発生主義 = an accrual basis. 発生した利益 = the amount of the profits arising in an accounting period のように, arising を使う
  def get_amount_arising(b, e, division = nil)
    raise TypeError if !b.is_a?(Date)
    
    t = JournalEntry.where('book_id = ? AND (date BETWEEN ? AND ?) AND account_id = ?', @book.id, b, e, @ac.id)
    t = t.where('division_id = ?', division.id) if division

    ret = t.select(<<EOS).take
SUM(CASE vat_excluded
      WHEN 0 THEN amount
      WHEN 1 THEN amount
      WHEN 2 THEN amount - vat_amt
    END) AS amount
EOS
    return (!ret || !ret.amount ? BigDecimal("0") : ret.amount)
  end

  
  # @param beginning B/S科目のとき nil, P/L科目のとき期首日
  # @param date      残高がほしい日付
  #
  # @return [BigDecimal] 金額。税抜経理の期間の分だけ消費税を抜く
  #
  # 例えば, 期首=4/1で 4月1日からのP/L元帳を表示したい
  #   => beginning = 4/1, date = 3/31  ... 単に 0 になる
  # 期首=4/1で, 翌3/31末の残高を得たい(P/L)
  #   => beginning = n/4/1, date = n+1/3/31
  def get_balance(beginning, date, division = nil)
    ret = BigDecimal("0")
    
    beginning = Date.new(1971, 1, 1)  if !beginning # とにかく古く
    amt = get_amount_arising(beginning, date, division)
    raise "internal error" if !amt
    return amt
  end

end
