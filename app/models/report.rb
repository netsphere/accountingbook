
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 帳票テンプレートマスタ
class Report < ApplicationRecord
  # システム帳票はナル値.
  belongs_to :book, optional: true

  validates_presence_of :name

  
  # 呼び出し側が `transaction()` をおこなうこと
  # @param pos = -1 の場合, 末尾に追加
  def insert_title pos, title
    raise ArgumentError if !pos || pos == 0
    
    if pos < 0 
      m = ReportingTitle.where('report_id = ?', self.id).order(position:"DESC").limit(1).take 
      title.position = m ? m.position + 1 : 1
    else
      rt = ReportingTitle.where('report_id = ? AND position >= ?',
                                self.id, pos).order(:position)
      # 歯抜けは気にしない。前から必要なところまで見て, 後ろから順に保存
      to_save = []
      rt.each_with_index do |r, idx|
        break if r.id == title.id || r.position > pos + idx
        r.position += 1; to_save.unshift r
      end
      to_save.each do |r| r.save! end
      
      title.position = pos
    end
    title.save!
  end
  
end
