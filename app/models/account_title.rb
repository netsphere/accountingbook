
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 勘定科目マスタ
class AccountTitle < ApplicationRecord
  include ActsAsDag
  acts_as_tree
  
  # システム科目は book_id IS NULL. 
  belongs_to :book, optional: true

  has_many :ac_ovrds
  def ac_ovrd(book)
    raise ArgumentError if self.book_id && self.book_id != book.id
    AcOvrd.where('book_id = ? AND account_title_id = ?', book.id, self.id).take
  end

  belongs_to :payable_ac, optional: true,
                          class_name:'AccountTitle', foreign_key:'payable_id'

  validates_presence_of :name
  #validates_presence_of :name_short
  def name_short
    self[:name_short].blank? ? self[:name] : self[:name_short]
  end
  
  # 1文字は不可
  validates_presence_of :sort
  validates :sort, format: {with: /\A[a-zA-Z][a-zA-Z0-9]+\z/}

  # 実在科目のみ: "元帳" の表示正残 +1: 借方, -1: 貸方
  validates_presence_of :sign

  # 実在科目のみ: 発生科目 (4) か残高科目 (1) か
  validates_presence_of :ac_type

  # @note `before_save()` では遅い。
  before_validation :check_fields
  
  KINDS = {1 => "販売請求書の債権科目", 2 => "(購買)請求書の債務科目",
           4 => "資金科目"}


  ##########################################################################
  # Class Methods
  
  # 集計科目の一覧. システム科目を含む
  # `options_from_collection_for_select()` の第1引数に渡す
  def self.aggregations(book)
    return AccountTitle #.left_outer_joins(:ac_ovrds)
               .where('(book_id IS NULL OR book_id = ?) AND aggregate IS TRUE', book.id)
               .order(:sort)
  end


  # @return   Active な実在科目のグループを返す.
  #
  # `<optgroup>` は多段にできない。単に無視され、1段階になる。
  # 振替伝票で `grouped_collection_select()` に渡す
  #    -> 内部で `option_groups_from_collection_for_select()` に渡される
  def self.grouped_items(book, only_real = true)
    require "ostruct"
    
    # 返すのはグループのほう。いったん簡単に, roots でグループ
    # 閉包テーブルは簡単.
    # `left_outer_joins()` には文字列は書けない。`joins()` を使え.
    ret = []
    AccountTitle.tree_roots(book).each do |root_ac|
      root = OpenStruct.new(name:root_ac.name, children:[], sort:root_ac.sort)
      root.children = root_ac.active_self_and_descendants(book)
      if only_real
        root.children = (root.children.filter do |r| !r.aggregate end)
      end
      # 集計科目しかなかったら削除
      ret << root if root.children.any? {|v| !v.aggregate }
    end

    return ret
  end


  # main tree の部分の root の一覧
  def self.tree_roots(book)
    AccountTitle.joins('LEFT JOIN (SELECT * FROM account_title_edges WHERE main_parent IS TRUE) ST ON account_titles.id = ST.child_id')
                #.left_outer_joins(:ac_ovrds)
                .where('parent_id IS NULL AND (account_titles.book_id IS NULL OR account_titles.book_id = ?)', book.id)
                .order('sort')
  end

  
  # 有効な債務科目の一覧
  def self.ap_items(book)
    AccountTitle.left_outer_joins(:ac_ovrds).where(<<EOS, book.id).order(:sort)
(account_titles.book_id IS NULL OR account_titles.book_id = ?) AND
    ac_ovrds.active IS TRUE AND
    kind = 2
EOS
  end

  
  ##########################################################################
  # Instance Methods

  def children_json(book, root)
    ret = []
    self.children.each do |node|
      next if node.book_id && node.book_id != book.id
      r = { id: node.id, name: node.name, aggregate: node.aggregate,
            root: root.id,
            sort: node.sort,
            show_link: "/books/#{book.id}/account_titles/#{node.id}",
            active: node.aggregate ? true :
                      (node.ac_ovrd(book) ? node.ac_ovrd(book).active : false),
            ac_type: node.ac_type,  # 1 = 残高, 4 = 発生
          }
      if !node.aggregate
        r[:tax_group] = node.tax_group
        r[:payable_ac] = node.enable_invoice ? node.payable_ac.name : "-"
      else
        r["_children"] = node.children_json(book, root) 
      end
      ret << r
    end
    return ret
  end

  
  # Root まで、順に辿って hierarchies 表に登録
  # `transaction()` は, 必要があれば, 呼出し側でおこなうこと
  def add_child child, additional_fields = {}
    raise ArgumentError if !self.aggregate
    
    h = self.class._aat.hierarchy_class
    # <s>直接の親と、親の先祖全部に張る。後者は、単に直接の親の `ancestors` を使えばいい。</s>
    h.create!( {main_parent: true}.merge(additional_fields)
                                  .merge(parent_id: self.id,
                                         child_id: child.id,
                                         #distance: 1
                                        ))
=begin
    self.ancestor_hierarchies.each do |ancestor_h|
      # ● TODO: ブロックを呼び出すようにしないと、追加フィールドが決められない
      h.create!(ancestor_id: ancestor_h.ancestor_id,
                descendant_id: child.id,
                distance: ancestor_h.distance + 1,
                main_parent: ancestor_h.main_parent, 
                sign_reversal: ancestor_h.sign_reversal ^ additional_fields[:sign_reversal] )
    end
=end
  end


  # active な子孫一覧. 合計科目は全部含める.
  def active_self_and_descendants book
    # `UNION ALL` だと重複してしまう?
    # 冒頭の `SELECT parent_id FROM account_title_edges` が複数行得られる
    #   -> それぞれについて `UNION` 以下が走る
    #      -> `SELECT DISTINCT` にして、`UNION ALL` に戻す
    # ここにある例を参照;
    #   https://cloud.google.com/bigquery/docs/recursive-ctes?hl=ja
    AccountTitle.find_by_sql([<<EOS, self.id, book.id, book.id])
WITH RECURSIVE self_and_descendants (ac_id) AS (
  SELECT DISTINCT parent_id FROM account_title_edges
      WHERE parent_id = ?
  UNION ALL 
  SELECT E.child_id
      FROM account_title_edges E, self_and_descendants AS sources 
      WHERE sources.ac_id = E.parent_id
  ) CYCLE ac_id SET isCycle USING path

SELECT account_titles.* FROM self_and_descendants
             JOIN account_titles ON self_and_descendants.ac_id = account_titles.id
             LEFT JOIN ac_ovrds ON account_titles.id = ac_ovrds.account_title_id
    WHERE (account_titles.book_id IS NULL OR account_titles.book_id = ?) AND
          (account_titles.aggregate IS TRUE OR (ac_ovrds.book_id = ? AND ac_ovrds.active IS TRUE))
    ORDER BY sort
EOS
  end
  
  
private
  # For `before_validation()`. 
  def check_fields
    self.name       = (self.name || "").unicode_normalize(:nfkc)
    self.name_short = (self.name_short || "").unicode_normalize(:nfkc)
    self.sort       = (self.sort || "").unicode_normalize(:nfkc).upcase
    self.tax_group  = self.tax_group || 0
    
    if aggregate
      # 集計科目
      if enable_invoice
        errors.add 'enable_invoice', "must be false when `aggregate` is true"
      end
      
      if kind && kind != 0
        errors.add "kind", "must be 0 when `aggregate` is true."
      end
      self.kind = 0
      self.sign = 0
      self.ac_type = 0
    else
      # 実在科目
      # 親がないのは不可 => コントローラで対処

      if sign != +1 && sign != -1
        errors.add 'sign', "must be +1 or -1, but '#{self.sign}'"
      end
      if ac_type != 1 && ac_type != 4
        errors.add 'ac_type', "must set, but '#{self.ac_type}'"
      end
      
      if enable_invoice
        if !payable_id
          errors.add 'payable_id', "must set accounts payable"
        end
      end
    end
  end
  
end
