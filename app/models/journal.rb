
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 振替伝票
class Journal
  include ActiveModel::Validations

  # `active_record/associations.rb` で定義されている。ここでは使えない
  #belongs_to :book

  validates :date, presence: true
  
  attr_reader :date
  def date=(val)
    @date = val.is_a?(Date) ? val : Date.parse(val)
  end
  
  attr_reader :entry_no
  def entry_no=(val)
    @entry_no = val.to_i
  end
  
  attr_accessor :entries

  def initialize params = {}
    self.date     = params[:date]
    self.entry_no = params[:entry_no]
    @entries      = params[:entries] || []
  end

  
  # @param ent_params [Hash of Hash]   各行. 配列ではなく {"0" => {...}, ...}
  # 保存はしない. 呼び出し側で保存すること
  def self.create_from_params book, journal_params, ent_params
    raise TypeError if !ent_params.respond_to?(:keys)
    
    ret = Journal.new( date: Date.parse(journal_params[:date]), 
                       entry_no: rand(200_000),   # TODO: ●●impl. 連番
                       entries: [] )
    
    # 1回回して、埋める
    balance = {}
    ent_params.each do |lineno, line|
      next if line[:account_id].blank?
      
      ac = AccountTitle.where('(book_id IS NULL OR book_id = ?) AND aggregate IS FALSE AND id = ?', book.id, line[:account_id]).take
      raise("internal error") if !ac

      # `blank?` は空白のみも true
      debit_amt  = !line[:debit_amt].blank? ? BigDecimal(line[:debit_amt].unicode_normalize(:nfkc)) : 0
      credit_amt = !line[:credit_amt].blank? ? BigDecimal(line[:credit_amt].unicode_normalize(:nfkc)) : 0
      if debit_amt != 0
        amt = debit_amt - credit_amt;     dr_or_cr = +1
      elsif credit_amt != 0
        amt = -(credit_amt - debit_amt);  dr_or_cr = -1
      else
        next 
      end
      balance[ac.id] = (balance[ac.id] || 0) + amt

      div = Division.find line[:division_id]

      # 部門が消費税不可の場合は, 税コード削除。免税期間は, 税コードはイキ。
      if line[:vat_id].to_i != 0 && !div.vat_disable
        vat = Vat.find(line[:vat_id].to_i)
        # 0に向かって丸める.

        # 免税期間か確認. 免税期間なら、税額だけ 0 にする
        period = PeriodEnd.get_period(book, ret.date)
        if period && period.vat_taxable > 0 && ret.date >= period.registration_of_taxable
          rate = vat.tax_rate / 100.0
          vat_amt = amt - (amt / (1.0 + rate)).round(0, BigDecimal::ROUND_UP)
          vat_excluded = period.vat_exclusive + 1 # 1 = 税込, 2 = 税抜
        else
          # 免税期間。コードはイキ
          vat_amt = 0
          vat_excluded = 0
        end
      else
        vat = nil
        vat_amt = 0
        vat_excluded = 0
      end
      je = JournalEntry.new(
                book_id:  book.id,
                date:     ret.date,
                entry_no: ret.entry_no,
                division_id: div.id,
                account_id: ac.id,
                partner_id: line[:partner_id],
                amount: amt,         # 正が借方, 負が貸方
                vat_amt: vat_amt,
                vat_id: vat ? vat.id : nil,
                comment: line[:comment].unicode_normalize(:nfc),
                dr_or_cr: dr_or_cr, # +1 が借方に表示, -1 が貸方に表示
                vat_excluded: vat_excluded,
                status: ""
           )  
      ret.entries << je
    end
    if (balance.reduce(0) do |sum, (key, value)| sum += value end) != 0
      raise "貸借不一致" # TODO impl.
    end
    
    # 相手勘定を特定
    dr_max = nil; cr_max = nil
    balance.each do |ac_id, value|
      if value > 0
        dr_max = ac_id if dr_max.nil? || value > balance[dr_max]
      elsif value < 0
        cr_max = ac_id if cr_max.nil? || value < balance[cr_max]
      else
        dr_max = ac_id if dr_max.nil?  # 空になるのを防ぐ
        cr_max = ac_id if cr_max.nil?
      end
    end
    
    # もう一回埋める
    ret.entries.each do |entry|
      entry.contra_ac_id = entry.amount > 0 ? cr_max :
                             (entry.amount < 0 ? dr_max : nil)
    end

    return ret
  end
  
end

