# -*- coding:utf-8 -*-

# (購買) 請求書の明細行
class InvoiceDetail < ApplicationRecord

  # 親
  belongs_to :invoice

  # 費用科目
  belongs_to :account_title

end
