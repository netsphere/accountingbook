# -*- coding: utf-8 -*-

# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# クレジットカードの引き落とし (預金と照合)
class CcReconciliation < ApplicationRecord
  # 親
  belongs_to :book

  # 支払い口座
  belongs_to :cash_account
  
  # 支払いの明細行
  has_many :cc_reconc_details

  validates_presence_of :date
  
end

