
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 会計部門
class Division < ApplicationRecord
  include ActsAsDag
  acts_as_tree

  # システム科目は book_id IS NULL. 
  belongs_to :book, optional: true
  
  has_many :div_ovrds
  def div_ovrd(book)
    raise ArgumentError if self.book_id && self.book_id != book.id
    DivOvrd.where('book_id = ? AND division_id = ?', book.id, self.id).take
  end
  
  validates_presence_of :name

  # 1文字は不可
  validates_presence_of :division_code
  validates :division_code, format: {with: /\A[a-zA-Z][a-zA-Z0-9]+\z/}
  validates :division_code, uniqueness: {scope:["division_code", "book_id"]}
            
  # @note `before_save()` では遅い。
  before_validation :check_fields


  ##########################################################################
  # Class Methods
  
  # 集計部門の一覧. システム部門を含む
  # `options_from_collection_for_select()` の第1引数に渡す
  def self.aggregations(book)
    return Division.where('(book_id IS NULL OR book_id = ?) AND aggregate IS TRUE', book.id)
                   .order(:division_code)
  end

  
  # 木の部分の root の一覧. 運用上, 一つだけ
  def self.tree_roots(book)
    Division.joins('LEFT JOIN (SELECT * FROM division_edges WHERE main_parent IS TRUE) ST ON divisions.id = ST.child_id')
            #.left_outer_joins(:div_ovrds)
            .where('parent_id IS NULL AND (divisions.book_id IS NULL OR divisions.book_id = ?)', book.id)
            .order('division_code')
  end


  # 会計部門のほうは、root が必ず一つ。いったん、グルーピングなしにする。
  # @return 実在科目のみの一覧
  def self.active_items(book, date)
    Division.joins(:div_ovrds)
            .where('(divisions.book_id IS NULL OR divisions.book_id = ?) AND (aggregate IS FALSE) AND (div_ovrds.book_id = ? AND (end_day IS NULL OR end_day > ?))', book.id, book.id, date)
  end

  
  ##########################################################################
  # Instance Methods
  
  def children_json(book)
    ret = []
    self.children.each do |node|
      next if node.book_id && node.book_id != book.id
      r = {  id: node.id, name: node.name,
             aggregate: node.aggregate,
             division_code: node.division_code,
             purchase_active: node.purchase_active,
             vat_disable: node.vat_disable,
             show: "/books/#{book.id}/divisions/#{node.id}",
             active: node.aggregate ? true :
                       (node.div_ovrd(book) ? node.div_ovrd(book).end_day == nil : false)
          }
      r["_children"] = node.children_json(book) if node.aggregate
      ret << r
    end
    return ret
  end

  
  # Root まで、順に辿って hierarchies 表に登録
  # `transaction()` は, 必要があれば, 呼出し側でおこなうこと
  def add_child child, additional_fields = {}
    raise ArgumentError if !self.aggregate

    h = self.class._aat.hierarchy_class
    # 直接の親と、親の先祖全部に張る。後者は、単に直接の親の `ancestors` を使えばいい。
    h.create!( {main_parent: true}.merge(additional_fields)
                                  .merge(parent_id: self.id,
                                         child_id: child.id,
                                         #distance: 1
                                        ))
=begin
    self.ancestor_hierarchies.each do |ancestor_h|
      # ● TODO: ブロックを呼び出すようにしないと、追加フィールドが決められない
      h.create!(ancestor_id: ancestor_h.ancestor_id,
                descendant_id: child.id,
                distance: ancestor_h.distance + 1,
                main_parent: ancestor_h.main_parent )
    end
=end
  end


private
  # for `before_validation()`. `before_save()` では遅い。
  def check_fields
    self.name          = (self.name || "").unicode_normalize(:nfkc)
    self.division_code = (self.division_code || "").unicode_normalize(:nfkc).upcase
  end
  
end
