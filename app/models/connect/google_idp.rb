
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class Connect::GoogleIdp
  def self.config
    return @config if @config

    # Ruby 3.1 で YAML (psych) 4.0.0 がバンドル。非互換.
    @config = YAML.load_file(Rails.root.to_s + "/config/connect/google.yml",
                             aliases: true)[Rails.env].symbolize_keys
    # discovery metadata のほうが優先
    @config.merge! OpenIDConnect::Discovery::Provider::Config.discover!(
                       @config[:issuer]
                     ).as_json
    if Rails.env.production?
      @config.merge!(
            client_id:     ENV['g_client_id'],
            client_secret: ENV['g_client_secret']
        )
    end
    return @config
  end

  def self.client
    @client ||= OpenIDConnect::Client.new(
        identifier:             config[:client_id],
        secret:                 config[:client_secret],
        authorization_endpoint: config[:authorization_endpoint],
        token_endpoint:         config[:token_endpoint],
        redirect_uri:           config[:redirect_uri]
      )
  end

    
  # @return Authentication Request URL
  def self.authorization_uri(options = {})
    # `options` のほうが優先
    client.authorization_uri( {
                scope: config[:scopes_supported]
    }.merge(options) )
  end
  
  def self.jwks
    @jwks ||= JSON::JWK::Set.new(#JSON.parse(
        OpenIDConnect.http_client.get(config[:jwks_uri]).body # これは Hash
      )#)
  end

  # Callback from `User.authenticate()`
  def self.authenticate(code, nonce)
    # token の検証
    client.authorization_code = code
    token = client.access_token! :secret_in_body  # ここで IdP にアクセス
    id_token = OpenIDConnect::ResponseObject::IdToken.decode(
        token.id_token, jwks
      )
    # 検証に失敗すると例外
    id_token.verify!({ :issuer => config[:issuer],
                         :nonce => nonce,
                         :client_id => config[:client_id] })

    # ログインするユーザを返す
    user = User.find_by_email id_token.raw_attributes[:email]
    # 既存ユーザ
    return user if user

    # Just-in-Time User Provisioning
    # ●● TODO: 利用規約の承認
    user = User.new(login: SecureRandom.alphanumeric(16),
                    #active: true,
                    #superuser: false,
                    name: id_token.raw_attributes[:name],
                    email: id_token.raw_attributes[:email],
                    admin_role: false
                    #timezone: "Asia/Tokyo",
                    #description: id_token.to_s
                   )
    user.save!
    #email.user_id = user.id
    #email.save!
    
    return user
  end

end

