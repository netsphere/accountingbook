
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 財務諸表ビジネスロジック
# DB ではない
class Statement
  def initialize(book)
    raise TypeError if !book.is_a?(Book)
    @book = book
  end

  # @param b, e  開始, 終了。いずれも含む
  # @return [Array of JournalEntry]  account_id, amount のみ
  def get_amounts_arising(b, e, division = nil)
    raise TypeError if !b.is_a?(Date)

    t = JournalEntry.where('book_id = ? AND (date BETWEEN ? AND ?)', @book.id, b, e)
    t = t.where('division_id = ?', division.id) if division

    ret = t.group("account_id").select(<<EOS).order(:account_id)
SUM(CASE vat_excluded
      WHEN 0 THEN amount
      WHEN 1 THEN amount
      WHEN 2 THEN amount - vat_amt
    END) AS amount, account_id
EOS
    return ret
  end
  
  # @param date 指定した日時点での残高, not 前期繰越 (BBF)
  # @return [Hash] {ac_id => [date までの累計額, 期首日〜dateまでの発生額]}
  #                残高科目であっても期首日は一律
  def get_balance(beginning, date, division = nil)
    # 全科目の累計
    t_all = get_amounts_arising(Date.new(1971, 1, 1), date, division)
    
    # 期首から指定日まで
    t_term = get_amounts_arising(beginning, date, division)

    ret = {}
    t_all.each do |r| ret[r.account_id] = [r.amount, BigDecimal("0.0")] end
    t_term.each do |r| ret[r.account_id][1] = r.amount end

    return ret
  end

  # 再帰
  def update_amt(ac, balance, arising)
    if ac.aggregate && !balance[ac.id]
      b = [BigDecimal("0.0"), BigDecimal("0.0")]
      a = BigDecimal("0.0")
  
      # 各頂点 vertex (node) に流れ込んでいる辺 edge (arc)
      # main_parent に限定しない.
      AccountTitleHierarchy.where('ancestor_id = ? AND distance = 1', ac.id).each do |h|
        update_amt(h.descendant, balance, arising)
        # TODO: bool よりも +1/-1 のほうがよかった。
        # XBRL も `link:calculationArc` 要素 `weight` 属性. 0.5 とか -2 も可!
        if h.sign_reversal 
          b[0] -= balance[h.descendant_id] ? balance[h.descendant_id][0] : 0;
          b[1] -= balance[h.descendant_id] ? balance[h.descendant_id][1] : 0
          a -= (arising[h.descendant_id] || 0)
        else
          b[0] += balance[h.descendant_id] ? balance[h.descendant_id][0] : 0;
          b[1] += balance[h.descendant_id] ? balance[h.descendant_id][1] : 0
          a += (arising[h.descendant_id] || 0)
        end
      end
      balance[ac.id] = b
      arising[ac.id] = a
    end
  end
  private :update_amt
  
  def make_cube(template, beginning, term_from, term_to, division)
    raise TypeError if !template.is_a?(Report)
  
    # 実在科目をつくる
    balance = get_balance(beginning, term_from - 1, division)

    arising = {}
    get_amounts_arising(term_from, term_to, division).each do |r|
      arising[r.account_id] = r.amount
    end
    
    ReportingTitle.where('report_id = ?', template.id).each do |t|
      update_amt(t.account_title, balance, arising)
    end

    return balance, arising
  end

end # class Statement

