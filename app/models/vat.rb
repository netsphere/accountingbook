
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 消費税マスタ
class Vat < ApplicationRecord

  # 'JP'
  validates_presence_of :country

  validates_presence_of :tax_code
  validates_uniqueness_of :tax_code

  validates_presence_of :name

  validates_presence_of :effective_day

  validates_presence_of :tax_rate

  validates_presence_of :tax_group

  # 非課税仕入は不課税(対象外) と区別しない。対称性なさすぎ.
  TAX_GROUP = {
    10 => "課税売上(標準)",
    20 => "課税売上(軽減)",
    50 => "非課税売上",
    60 => "輸出免税",
    210 => "課税仕入(標準)",
    220 => "課税仕入(軽減)",
    250 => "非課税仕入",  # 勘定科目側だけ、利用。
    270 => "ﾘﾊﾞｰｽﾁｬｰｼﾞ" }

  
  ##########################################################################
  # Class Methods
  
  # グルーピングなしで返す
  def self.active_items(book, date)
    Vat.where('effective_day <= ?', date)
       .order(:tax_group).order(effective_day: :desc)
  end

end


# 固定資産の売却

=begin
   CASH          100  /  固定資産の売却収入 100 (9)

   固定資産の売却原価 700 / 固定資産の取得価額 1,000
   減価償却累計額     300

   => 固定資産売却損 = 91 - 700 = 609

=end
