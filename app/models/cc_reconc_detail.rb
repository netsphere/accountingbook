
# クレジットカードの消し込みの明細行
class CcReconcDetail < ApplicationRecord
  # 親
  belongs_to :cc_reconciliation

  # 経費
  belongs_to :expense

  validates_presence_of :amount
end
