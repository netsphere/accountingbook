
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 会計期間
# × 期中に免税事業者から課税事業者に切り替わった場合は, 年度を分けること。
#    -> ダメ。償却計算に影響が出る。
class PeriodEnd < ApplicationRecord
  belongs_to :book

  # 期末日
  validates :date, presence:true
  
  validates :vat_taxable, presence:true
  
  # 0: 免税・税込経理, 1: 税抜経理
  validates :vat_exclusive, presence:true
  
  validate :check

  VAT_TAXABLE = {0 => "免税", 1 => "課税 (本則課税)", 2 => "課税 (簡易課税)",
                 3 => "小規模事業者の2割特例"}
  
  # @return 期首日. = 一つ前の期の期末日 + 1. nil のことがある
  def beginning
    prev = PeriodEnd.where('book_id = ? AND date < ?', book.id, date)
                    .order('date': "DESC").limit(1).take
    return (prev ? prev.date + 1 : nil)
  end

  
  # @return   `nil` のことがある
  def self.get_period(book, date)
    return where('book_id = ? AND date >= ?', book.id, date)
              .order('date').limit(1).take
  end

  
private
  # for `validate`
  def check
    # 期末日
    if !self.date.nil?
      n = (date >> 1)
      if date.day >= 28 && date < Date.new(n.year, n.mon, 1) - 1
        errors.add(:date, "28日以降ですが月末日ではありません。")
      end
    end

    if self.registration_of_taxable.blank?
      if self.vat_taxable != 0 || self.vat_exclusive != 0
        errors.add(:base, "課税期間と課税事業者, 税抜経理? の組み合わせを再確認ください。")
      end
    else
      if registration_of_taxable >= date
        errors.add(:registration_of_taxable, "日付が誤っています")
      end
      
      if self.vat_taxable == 0 
        errors.add(:base, "課税期間と課税事業者, 税抜経理? の組み合わせを再確認ください。")
      end
    end
  end
end
