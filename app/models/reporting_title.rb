
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 報告様式の各科目
class ReportingTitle < ApplicationRecord
  # 親
  belongs_to :report

  belongs_to :account_title

  # 表示正残. 例えば減価償却費は, P/L ではマイナス, C/F ではプラス
  validates_presence_of :sign

  # 発生科目 (4) か残高科目 (1) か. 仕入債務は, 資金繰り表では発生科目
  validates_presence_of :ac_type

  # @note `before_save()` では遅い。
  before_validation :check_fields

  
private
  # For `before_validation()`. 
  def check_fields
    if sign != +1 && sign != -1
      errors.add 'sign', "must be +1 or -1, but '#{self.sign}'"
    end
    if ac_type != 1 && ac_type != 4
      errors.add 'ac_type', "must set, but '#{self.ac_type}'"
    end
  end
  
end
