# -*- coding:utf-8 -*-

# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 会計帳簿のの設定 (特殊な勘定科目、など)
# v3: 3.0/models/user_setting.rb
class BookSetting < ApplicationRecord
  # 主キーなしはサポートされない。何か設定する
  self.primary_key = :book_id

  # 親
  belongs_to :book
  
  belongs_to :created_by, :foreign_key => "create_user_code",
             :class_name => "User", :primary_key => "login"
  belongs_to :updated_by, :foreign_key => "update_user_code",
             :class_name => "User", :primary_key => "login"

  
  # 仕訳追加可能期間
  def get_addable_period()
    return [fixed_date + 1, (fixed_date + 1) + 90]
  end


  # 締め日の翌日から次の月末日まで
  def get_ac_month()
    begin_date = fixed_date + 1
    
    if begin_date.day <= closing_day
      y = begin_date.year
      m = begin_date.mon
    else
      y = next_month(begin_date).year
      m = next_month(begin_date).mon
    end
    d = closing_day

    while !Date.valid_date?(y, m, d)
      d -= 1
    end

    return [begin_date, Date.new(y, m, d)]
  end
  
end # class BookSetting
