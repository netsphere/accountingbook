
# Q's Web Accounting Book
# Copyright (c) 2002-2007,2010-2011,2013,2024 Hisashi HORIKAWA. All rights reserved.
#   https://www.nslabs.jp/ac/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# 汎用の木を目指さない。勘定科目と会計部門の共通機能

# 有向非巡回グラフ (DAG), ただし、ベースは木
# `main_parent` を持つ
# @note クラス名は `Concerns::` ではない.
module ActsAsDag
  
  # 内部クラス
  class TreeSupport
    attr_reader :model_class
    attr_reader :options
  
    attr_reader :hierarchy_class_name
    attr_reader :hierarchy_class
  
    def initialize model_class, options
      @model_class = model_class
      @options = {}.merge(options)

      @hierarchy_class_name = @options[:hierarchy_class_name] ||
                            @model_class.to_s + "Edge"
      @hierarchy_class = Module.const_get(@hierarchy_class_name)
    end

  end


  def self.included(base)
    # クラスメソッドを生やす
    base.extend ClassMethods
  end

=begin
  # 直接の親。複数ある
  def parents
    self.class.joins(:descendant_hierarchies).where('descendant_id = ? AND distance = 1', self.id)
  end

  
  def children
    self.class.joins(:ancestor_hierarchies).where('ancestor_id = ? AND distance = 1', self.id)
  end
=end
  
  
  # 直接の主たる親
  def main_parent
    #raise "Hoge"
    pair = self.class._aat.hierarchy_class.where('child_id = ? AND main_parent IS TRUE', self.id).take
    return (pair ? pair.parent : nil)
  end

  
  module ClassMethods
    attr_reader :_aat
    
    def acts_as_tree options = {}
      options.assert_valid_keys(
        :hierarchy_class_name
      )

      # @note ここでクラス変数を使ってはならない。派生クラス間も共有される.
      @_aat = TreeSupport.new self, options
      
      # 親が複数ある (木ではない).
      # @note 中間テーブルに別の名前を付けると区別できる.
      has_many :ancestor_hierarchies, class_name: _aat.hierarchy_class_name,
                                      foreign_key: "child_id"
      has_many :parents, through: :ancestor_hierarchies,
                           source: "parent"

      has_many :descendant_hierarchies, class_name: _aat.hierarchy_class_name,
                                        foreign_key: "parent_id"
      has_many :children, through: :descendant_hierarchies,
                             source: "child"
    end

  end # ClassMethods
end


