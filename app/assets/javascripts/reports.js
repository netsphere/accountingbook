"use strict";

function renumber_pos()
{
    $('.row_pos').each (function (i) {
        $(this).val(i + 1);
    });
}

/* 報告様式に追加する */
function to_report( ac_id )
{
    var li = $("li[data-id='" + ac_id + "']")[0];
    //alert( li.text() );
    var lines_tag = $('#lines');
    var lines = Number(lines_tag.val()); lines_tag.val(lines + 1);

    // TODO: HTML片を Ajax にする.
    $('#rightList tbody > tr:last').after(
        '<tr><td><input type="hidden" name="rows[' + lines + '][account_title_id]" value="' + ac_id + '" />' +
        li.firstChild.nodeValue + '</td>' +
        '<td><input type="hidden" name="rows[' + lines + '][position]" class="row_pos" />' +
        '<td nowrap>' +
        '<button type="button" onclick="up_row(this)">↑</button>' +
        '<button type="button" onclick="down_row(this)">↓</button> |' +
        '<button type="button" onclick="del_row(this)">×</button>' );
    renumber_pos();
}


function up_row( self )
{
    //alert('up');
    var row =  $(self).closest('tr');
    var rowIndex = $('#rightList tbody > tr').index(row);
    if (rowIndex > 1 )
        row.insertBefore( row.prev('tr') );
    renumber_pos();
}

function down_row( self )
{
    //alert('down');
    var row = $(self).closest('tr');
    if (row.next.length) {
        row.insertAfter( row.next('tr') );
    }
    renumber_pos();
}

function del_row( self )
{
    //alert('del');
    var row = $(self).closest('tr');
    row.remove();
    renumber_pos();
}

