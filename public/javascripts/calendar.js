
var today;     // 今日
var cur_first; // 現在月の1日
var week = new Array('日', '月', '火', '水', '木', '金', '土');
var subWin;
var year_obj;
var mon_obj;
var day_obj;

/**
 * カレンダーHTMLの生成
 */
function getCalendarHtml() {
  var start_day = new Date(cur_first - (cur_first.getDay() * 1000 * 60 * 60 * 24));
  var title = cur_first.getFullYear() + "年" + (cur_first.getMonth() + 1) + "月";

  var ddata  = '<html><head>\n'
  ddata += '<title>' + title + '</title>\n'
  ddata += '<style type="text/css">html, body, form, table {padding:0; margin:0;}</style>\n'
  ddata += '<script type="text/javascript">function f() { self.window.focus(); }</script>\n'
  ddata += '</head>\n'
  ddata += '<body>\n'
  ddata += '<form>\n'
  ddata += '<table BGCOLOR="#dddddd" WIDTH="100%" '
  ddata +=     'STYLE="font-family:sans-serif; font-size:12px; text-align:right">\n'

  // 月
  ddata += '<tr BGCOLOR="#6699ff">\n'
  ddata += '<th COLSPAN=7 ALIGN="center" nowrap>\n'
  ddata += '<INPUT TYPE="button" VALUE="<<" onClick="window.opener.rewriteCalendar(-1)" '
  ddata +=     'style="padding:0">\n'
  ddata += '<span style="font-size:14px">' + title + '</span>\n'
  ddata += '<INPUT TYPE="button" VALUE="今月" onClick="window.opener.rewriteCalendar(0)" '
  ddata +=     'style="padding:0">\n'
  ddata += '<INPUT TYPE="button" VALUE=">>" onClick="window.opener.rewriteCalendar(1)" '
  ddata +=     'style="padding:0">\n'
  ddata += '</th>\n'
  ddata += '</tr>\n'

  // 週
  ddata += '<TR BGCOLOR="#00cccc">\n'
  for (var i = 0; i < 7; i++) {
    ddata += '<TH WIDTH=14>\n'
    ddata += week[i]
    ddata += '</TH>\n'
  }
  ddata += '</TR>\n'

  // 日
  for (var j = 0; j < 6; j++) {
    ddata += '<TR BGCOLOR="#eeeeee">\n'
    for (var i = 0; i < 7; i++) {
      var d = new Date(start_day.getTime() + ((j * 7 + i) * 1000 * 60 * 60 * 24));
      if (d.getFullYear() == today.getFullYear() && 
          d.getMonth() == today.getMonth() &&
          d.getDate() == today.getDate()) {
        ddata += '<TD BGCOLOR="#ff99ff" WIDTH=14>\n'
      }
      else if (d.getMonth() != cur_first.getMonth()) {
        ddata += '<TD BGCOLOR="#cccccc" WIDTH=14>\n'
      }
      else {
        ddata += '<td width=14>\n'
      }
      ddata += '<a href="javascript:window.opener.setValue(' + d.getFullYear() + ',' +
                 d.getMonth() + ',' + d.getDate() + ')">'
      ddata += d.getDate() 
      ddata += '</a>\n'
    }
    ddata += '</tr>\n'
  }

  // ステータス行
  ddata += '<tr>\n'
  ddata += '<td colspan="7" align="center">\n'
  ddata += '<input type="button" value="キャンセル" '
  ddata +=      'onClick="window.opener.closeCalendar();">\n'
  ddata += '</td>\n'
  ddata += '</tr>\n'
  ddata += '</table>\n'
  ddata += '</form></body></html>\n'

  return ddata;
}


/**
 * カレンダーウィンドウを開く
 */
function openCalendar(year_obj_, mon_obj_, day_obj_, ev, dateType) {
  today = new Date();
  cur_first = new Date(today.getFullYear(), today.getMonth(), 1);
  year_obj = year_obj_;
  mon_obj = mon_obj_;
  day_obj = day_obj_;

  var left = ev.screenX + 10;
  var top = ev.screenY - 50;

  subWin = window.open("", "calendar", 'width=190,height=195,left=' + left + ',top=' + top);
  subWin.document.write(getCalendarHtml());
  subWin.document.close();
  subWin.f();
}

function closeCalendar() {
  subWin.close();
}


function rewriteCalendar(direction) {
  var y = cur_first.getFullYear();
  var m = cur_first.getMonth();
  if (direction == -1) {
    // 前月
    if (m == 0) { 
      y -= 1; m = 11; 
    } 
    else 
      m -= 1;
  }
  else if (direction == 0) {
    y = today.getFullYear();
    m = today.getMonth();
  }
  else {
    // 次月
    if (m == 11) { 
      y += 1; m = 0; 
    } 
    else 
      m += 1;
  }
  cur_first = new Date(y, m, 1);
  subWin.document.open();
  subWin.document.write(getCalendarHtml());
  subWin.document.close();
}


function setValue(y, m, d) {
  if (year_obj) { year_obj.value = y; }
  mon_obj.value = m + 1;
  day_obj.value = d;
  subWin.close();
}

